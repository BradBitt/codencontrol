#define AppName "CodeNControl"
#define AppVersion "1.0"
#define AppVersionVersion "1.0.0.0"
#define AppPublisher "Tenuta"
#define AppIcoImage "Exe_Logo_250x250.ico"
#define AppBmpImage "Exe_Logo_64x64.bmp"

[Setup]
AppId={{4D2A4622-91A0-48AD-9D6D-788FE91F31AB}}
AppName={#AppName}
AppVersion={#AppVersion}
WizardStyle=modern
DefaultDirName={sd}\Tenuta\{#AppName}
UninstallDisplayIcon=./resources/{#AppIcoImage}
SetupIconFile=./resources/{#AppIcoImage}
OutputDir=./bin/
AppPublisher={#AppPublisher}
VersionInfoVersion={#AppVersionVersion}
OutputBaseFilename={#AppName}
WizardSmallImageFile=./resources/{#AppBmpImage}

[Files]
Source: "./resources/{#AppName}.exe"; DestDir: "{app}"

[Dirs]
Name: "{app}/cache"
Name: "{app}/log"

[Registry]
Root: HKLM; Subkey: "Software\{#AppPublisher}"; Flags: uninsdeletekeyifempty
Root: HKLM; Subkey: "Software\{#AppPublisher}\{#AppName}"; Flags: uninsdeletekeyifempty;
Root: HKLM; Subkey: "Software\{#AppPublisher}\{#AppName}\{#AppVersion}"; Flags: uninsdeletekey; ValueType: string; ValueName: "InstallationPath"; ValueData: "{app}"
Root: HKLM; Subkey: "Software\{#AppPublisher}\{#AppName}\{#AppVersion}"; Flags: uninsdeletekey; ValueType: string; ValueName: "CachePath"; ValueData: "{app}/cache"
Root: HKLM; Subkey: "Software\{#AppPublisher}\{#AppName}\{#AppVersion}"; Flags: uninsdeletekey; ValueType: string; ValueName: "LogPath"; ValueData: "{app}/log"