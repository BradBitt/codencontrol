package com.codencontrol.core.util;

public final class Constants {
	
	public static String APPLICATION_VERSION = "1.0";
	public static String PERSISTANT_DATA_INI_TAG = "persistant";
	public static String PROPERTIES_DATA_INI_TAG = "properties";
	public static String PERSISTANT_DATA_FILE_EXTENSION = "ini";
	public static String LOG_FILE_EXTENSION = "txt";
	
	// View XML reading
	public static String VIEW_XML_FILE_PATH = "/views.xml";
	public static String VIEW_XML_VIEW_TAG = "view";
	public static String VIEW_XML_NAME_ATTRIBUTE = "name";
	public static String VIEW_XML_POS_ATTRIBUTE = "pos";
	public static String VIEW_XML_PROJECT_NAME_ATTRIBUTE = "projname";
	
	// View property data keys
	public static String VIEW_PROPERTY_DATA_OPEN_STATE = "openstate";
	public static String VIEW_PROPERTY_DATA_CONTAINER_POSITION = "containerposition";
	
	// View property data default values
	public static String VIEW_PROPERTY_DATA_CONTAINER_POSITION_CLOSED = "null";
	
	private Constants() {}
}