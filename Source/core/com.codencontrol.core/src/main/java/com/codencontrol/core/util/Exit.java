package com.codencontrol.core.util;

import java.util.ArrayList;

import com.codencontrol.core.ViewManager;
import com.codencontrol.core.data.Save;
import com.codencontrol.ui.factory.GuiFactory;
import com.codencontrol.ui.toolbar.ProgressBar;
import com.codencontrol.ui.view.UniqueView;

/**
 * This is in charge of closing the application.
 * @author Bradley
 */
public class Exit {

	/**
	 * This function closes the application safely.
	 * This makes sure that all data is saved and any other things are done before the 
	 * application is closed.
	 */
	public static void safeQuit() {
		// Gets a list of all the unique views.
		ArrayList<UniqueView> allUniqueViews = ViewManager.getInstance().getUniqueViews();
		
		// Loops through all the unique views, runs the close function for
		// each view and then runs the save function for each one.
		int counter = 0;
		for (UniqueView uniqueView : allUniqueViews) {
			// Progress bar progress
			counter++;
			ProgressBar.getInstance().setProgress(counter / allUniqueViews.size() * 100);
			
			uniqueView.onCloseFunction();
			Save.saveViewData(uniqueView);
		}

		// Resets the progress bar and closes the stage.
		ProgressBar.getInstance().resetProgress();
		GuiFactory.getInstance().getMainStage().close();
	}
}