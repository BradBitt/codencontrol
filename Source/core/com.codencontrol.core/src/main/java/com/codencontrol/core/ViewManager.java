package com.codencontrol.core;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.reflections.Reflections;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.codencontrol.core.data.ViewXMLReader;
import com.codencontrol.core.util.Constants;
import com.codencontrol.ui.dialog.PopUp;
import com.codencontrol.ui.view.MainViewContainer;
import com.codencontrol.ui.view.UniqueView;

import javafx.scene.control.Alert;

/**
 * The view manager contains all the views that are used in the application.
 * When the application starts, this class finds all the views and adds them to a list.
 * 
 * @author Bradley
 */
public class ViewManager {
	
	private ArrayList<UniqueView> allUniqueViews = new ArrayList<>();
	private static ViewManager instance = null;
	
	/**
	 * This class is a singleton and so only contains one instance of it.
	 * @return ViewManager
	 */
	public static ViewManager getInstance() {
		if (instance == null) {
			instance = new ViewManager();
		}
		return instance;
	}
	
	/**
	 * The creation of a view manager finds all the views in the
	 * application and adds them to a list.
	 */
	private ViewManager() {
		allUniqueViews = findViewObjects();
	}
	
	/**
	 * This function finds all the views that should be loaded into
	 * the application and returns the list.
	 * @return ArrayList<UniqueView>
	 */
	private ArrayList<UniqueView> findViewObjects() {
		ArrayList<UniqueView> allViewObjects = new ArrayList<>();
		
		// Reads in all the views from the xml file.  
		ViewXMLReader viewReader = new ViewXMLReader(this.getClass().getResourceAsStream(Constants.VIEW_XML_FILE_PATH));
		NodeList viewNodeList = viewReader.getTagList(Constants.VIEW_XML_VIEW_TAG);
		
		// Gets the element nodes.
		List<Element> elementList = viewReader.getElementsFromNodes(viewNodeList);
		
		// Loops through the elements and gets the attributes
		for (Element elementNode : elementList) {
			String viewProjectName = elementNode.getAttribute(Constants.VIEW_XML_PROJECT_NAME_ATTRIBUTE);
			String viewPos = elementNode.getAttribute(Constants.VIEW_XML_POS_ATTRIBUTE);
			String viewName = elementNode.getAttribute(Constants.VIEW_XML_NAME_ATTRIBUTE);
			
			// Checks they were found.
        	if (viewProjectName == null || viewPos == null || viewName == null) {
        		PopUp.showPopUp(Alert.AlertType.ERROR, "Could not find view in 'views.xml'",
        				"Was unable to load the view object as its xml attributes were incorrect.");
        		continue;
        	}
        	
        	// Creates the view from the class object and then adds it to the list.
        	UniqueView view = createViewInstance(viewProjectName, viewPos, viewName);
        	if (view != null) {
        		allViewObjects.add(view);
        	}
		}
		
		return allViewObjects;
	}
	
	/**
	 * This function creates an instance of a view using the given parameters.
	 * It searches the project for classes that extend 'UniqueView' and then
	 * creates the view of that class.
	 * @param viewProjectName
	 * @param viewPos
	 * @param viewName
	 * @return UniqueView
	 */
	private UniqueView createViewInstance(String viewProjectName, String viewPos, String viewName) {
		// Finds the class that extends UniqueView
    	Reflections reflections = new Reflections(viewProjectName);    
		Set<Class<? extends UniqueView>> classes = reflections.getSubTypesOf(UniqueView.class);
		
		// There should only be one class extending the UniqueView
		if (classes.size() != 1) {
			PopUp.showPopUp(Alert.AlertType.ERROR, "Problem with View Project",
    				"There should only be one class that extends 'UniqueView'"
    				+ " in your view project or the class that extends this, could not be found.");
			return null;
		}
		
		UniqueView view = null;
		try {
			// Loops through the list of classes and creates an instance of the class and adds
			// it to the list of view classes. List should only contain 1 class.
			for (Class<? extends UniqueView> viewClass : classes) {
				view = viewClass.getDeclaredConstructor(String.class,
							MainViewContainer.ContainerPosition.class).newInstance(
									viewName, MainViewContainer.ContainerPosition.valueOf(viewPos));
			}
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException | NoSuchMethodException | SecurityException e) {
			PopUp.showPopUpWithException(Alert.AlertType.ERROR, "Failed to create Class object",
    				"Failed to create instance of 'UniqueView' with the class found", e);
		}
		
		return view;
	}
	
	/**
	 * Returns a list of all the unique views.
	 * @return ArrayList<UniqueView>
	 */
	public ArrayList<UniqueView> getUniqueViews() {
		return allUniqueViews;
	}
}