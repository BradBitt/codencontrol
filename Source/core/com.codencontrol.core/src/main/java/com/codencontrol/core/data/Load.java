package com.codencontrol.core.data;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.ini4j.Ini;

import com.codencontrol.core.util.Constants;
import com.codencontrol.core.util.RegistryValues;
import com.codencontrol.ui.dialog.PopUp;
import com.codencontrol.ui.view.MainView;
import com.codencontrol.ui.view.MainViewContainer;

import javafx.scene.control.Alert;

public class Load {
	
	/**
	 * This function loads the views persistent data.
	 * It then returns that persistent data as a map.
	 * @param view
	 * @return Map<String, String>
	 */
	public static Map<String, String> getViewPersistantData(MainView view) { 
		File iniFile = getIniFile(view.getName());
		
		// If the previous data doesn't exist then return.
		if (!iniFile.exists()) {
			return new HashMap<String, String>();
		}
		
		// Loads the ini persistent contents.
		try {
			Ini ini = new Ini();
			ini.load(new FileReader(iniFile));
			return ini.get(Constants.PERSISTANT_DATA_INI_TAG);
		} catch (IOException e) {
			PopUp.showPopUpWithException(Alert.AlertType.ERROR, "Failed to load data!",
					"Failed to load the persistant data for view: " + view.getName(), e);
			return new HashMap<String, String>();
		}
	}
	
	/**
	 * This function loads the views properties from its ini file
	 * if it exists.
	 * @param view
	 */
	public static void setViewProperties(MainView view) {
		File iniFile = getIniFile(view.getName());
		
		// If the previous data doesn't exist then return.
		if (!iniFile.exists()) {
			return;
		}
		
		// Loads the ini properties
		Ini ini = new Ini();
		try {
			ini.load(new FileReader(iniFile));
			Map<String, String> propertiesMap = ini.get(Constants.PROPERTIES_DATA_INI_TAG);
			// Sets the view state
			view.setViewState(Boolean.parseBoolean(propertiesMap.get(Constants.VIEW_PROPERTY_DATA_OPEN_STATE)));
			
			// Only sets the view position if the view was visible when the application was closed.
			if (view.isViewVisible()) {
				// Sets the view position
				view.setPreviousPosition(MainViewContainer.ContainerPosition.valueOf(
						propertiesMap.get(Constants.VIEW_PROPERTY_DATA_CONTAINER_POSITION)));
			}
			
		} catch (IOException e) {
			PopUp.showPopUpWithException(Alert.AlertType.ERROR, "Failed to load ini files!",
					"Failed to load the ini files that contain the view data.", e);
		}
	}
	
	/**
	 * This function gets the ini file for the specific view.
	 * The ini files are named after the view.
	 * @param viewName
	 * @return File
	 */
	private static File getIniFile(String viewName) {
		// Gets the location to load the data in.
		String fileDirectory = RegistryValues.getInstance().getChachePath();
		
		// Creates the file object.
		return new File(fileDirectory + File.separator
				+ viewName + "." + Constants.PERSISTANT_DATA_FILE_EXTENSION);
	}
}