package com.codencontrol.core.data;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Map;
import java.util.Set;

import com.codencontrol.core.util.Constants;
import com.codencontrol.core.util.RegistryValues;
import com.codencontrol.ui.dialog.PopUp;
import com.codencontrol.ui.util.ViewUtil;
import com.codencontrol.ui.view.MainView;
import com.codencontrol.ui.view.MainViewContainer;

import javafx.scene.control.Alert;

public class Save {
	
	/**
	 * This function takes a map of strings and saves them.
	 * The map should include data that will remain persistent in the view.
	 * This function is called when the program is closed and also when the view
	 * is closed.
	 * @param MainView
	 */
	public static void saveViewData(MainView view) {
		// Gets the location to save the data in.
		String fileDirectory = RegistryValues.getInstance().getChachePath();
		
		// Creates the file object.
		File persistentDataFile = new File(fileDirectory + File.separator
				+ view.getName() + "." + Constants.PERSISTANT_DATA_FILE_EXTENSION);
		
		// If the previous data exists then removes it.
		if (persistentDataFile.exists()) {
			persistentDataFile.delete();
		}
		
		// Writes the data to file.
		try {
			writeToFile(persistentDataFile, view);
		} catch (FileNotFoundException e) {
			PopUp.showPopUpWithException(Alert.AlertType.ERROR, "Failed to save data!",
					"Failed to save the persistant data for view: " + view.getName(), e);
		}
	}
	
	/**
	 * This function writes the data in the map to the file given.
	 * It writes in the format of an ini file.
	 * @param persistentDataFile
	 * @param mapData
	 * @throws FileNotFoundException
	 */
	private static void writeToFile(File persistentDataFile, MainView view) throws FileNotFoundException {
		PrintWriter writer = new PrintWriter(persistentDataFile);
		
		// Writes the view properties.
		writer.println("[" + Constants.PROPERTIES_DATA_INI_TAG + "]");
		// Writes the view properties to the file.
		writePropertiesToFile(view, writer);
		
		// Writes an empty line.
		writer.println();
		
		// Writes the ini tag for persistent data
		writer.println("[" + Constants.PERSISTANT_DATA_INI_TAG + "]");
		// Writes all view persistent data to the file.
		writePersistantToFile(view, writer);
		
		writer.close();
	}
	
	/**
	 * This function saves all the view properties to file.
	 * @param view
	 * @param writer
	 */
	private static void writePropertiesToFile(MainView view, PrintWriter writer) {
		
		// Writes the view state to file.
		writer.println(Constants.VIEW_PROPERTY_DATA_OPEN_STATE + "=" + view.isViewVisible().toString());
		
		// Saves the container position to the view properties.
		// If the view is not open then save 'null' to the properties file.
		// If container is null then the view is not open.
		// Writes the position to file.
		if (view.isViewVisible()) {
			MainViewContainer container = ViewUtil.findViewInContainers(view);
			if (container != null) {
				writer.println(Constants.VIEW_PROPERTY_DATA_CONTAINER_POSITION + "=" + container.getViewPosition().toString());
			} else {
				PopUp.showPopUp(Alert.AlertType.ERROR, "Failed to save data!",
						"Failed to find view container position for: " + view.getName());
			}
		} else {
			writer.println(Constants.VIEW_PROPERTY_DATA_CONTAINER_POSITION + "=" + Constants.VIEW_PROPERTY_DATA_CONTAINER_POSITION_CLOSED);
		}
	}
	
	/**
	 * This function writes all the persistent data to file.
	 * This is data that the developer of the view wants to be saved
	 * in between uses of the view.
	 * @param view
	 * @param writer
	 */
	private static void writePersistantToFile(MainView view, PrintWriter writer) {
		Map<String, String> mapData = view.getViewMapData();
		
		Set<String> allKeys = mapData.keySet();
		for(String key : allKeys) {
			writer.println(key + "=" + mapData.get(key));
		}
	}
}