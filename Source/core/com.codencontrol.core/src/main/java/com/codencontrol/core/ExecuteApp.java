package com.codencontrol.core;

import com.codencontrol.ui.dialog.PopUp;
import com.codencontrol.ui.factory.GuiFactory;
import com.codencontrol.ui.view.UniqueView;

import java.util.ArrayList;

import com.codencontrol.core.data.Load;
import com.codencontrol.core.util.Exit;
import com.codencontrol.core.util.Log;
import com.codencontrol.core.util.RegistryValues;

import javafx.application.Application;
import javafx.scene.control.Alert;
import javafx.stage.Stage;

public class ExecuteApp extends Application {
	
	/**
	 * This function is called when the executable is launched.
	 * @param args
	 */
	public static void main(String[] args) {
		launch(args);
	}
	
	/**
	 * This function kicks things off.
	 */
	@Override
	public void start(Stage primaryStage) {
		try {
			// Create log file and redirects standard error
			String logDir = RegistryValues.getInstance().getLogPath();
			Log.getInstance().redirect(logDir);
			
			// Creates the main GUI
			GuiFactory.getInstance().buildGui();
			
			// Gets all views and creates them
			ArrayList<UniqueView> allUniqueViews = ViewManager.getInstance().getUniqueViews();
			
			// Loads the views data, default buttons and menu items
			GuiFactory.getInstance().loadDefaultGui(allUniqueViews);
			
			// Loads all the views saved data
			for (UniqueView uniqueView : allUniqueViews) {
				uniqueView.setViewMapData(Load.getViewPersistantData(uniqueView));
				Load.setViewProperties(uniqueView);
				GuiFactory.getInstance().getViewFactory().buildView(uniqueView);
			}
			
			// Shows the GUI
			GuiFactory.getInstance().showGui();

		} catch(Exception e) {
			PopUp.showPopUpWithException(Alert.AlertType.ERROR, "Application Fail", 
					"The application failed to load!" + "please check the error exception below.", e);
		}
	}
	
	/**
	 * This function is called when the user clicks the
	 * red X button on the main stage.
	 */
	@Override
	public void stop(){
		Exit.safeQuit();
	}
}