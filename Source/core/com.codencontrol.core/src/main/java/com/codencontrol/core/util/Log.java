package com.codencontrol.core.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.codencontrol.ui.dialog.PopUp;

import javafx.scene.control.Alert;

public class Log {
	
	private static Log instance = null;
	
	private String logFileName;
	private String logDir;
	
	public static Log getInstance() {
		if (instance == null) {
			instance = new Log();
		}
		return instance;
	}
	
	/**
	 * This creates the log file and changes
	 * the error and output to point to the log file.
	 */
	public void redirect(String logDir) {
		// Creates LogFile
		try {
			this.logDir = logDir;
			this.logFileName = "log-" + getDateForLog() + "." + Constants.LOG_FILE_EXTENSION;
			File logFile = new File(logDir + File.separator + logFileName);
			
			FileOutputStream fos = new FileOutputStream(logFile);
			PrintStream ps = new PrintStream(fos);
			
			// Sets error and output to log file.
			System.setErr(ps);
			System.setOut(ps);
			
		} catch (FileNotFoundException e) {
			PopUp.showPopUpWithException(Alert.AlertType.ERROR, "Failed to create log file!",
					"Failed to Create the log file and redirect standard error and output to log file.", e);
		}
	}
	
	/**
	 * Returns the name of the log file.
	 * @return String
	 */
	public String getLogName() {
		return logFileName;
	}
	
	/**
	 * Returns the directory of the log file.
	 * @return String
	 */
	public String getLogDir() {
		return logDir;
	}
	
	/**
	 * This returns the date and time in a specific format
	 * suited for the log file name.
	 * @return String
	 */
	private String getDateForLog() {
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yy_HH-mm-ss");  
	    Date date = new Date();  
	    return formatter.format(date); 
	}
}