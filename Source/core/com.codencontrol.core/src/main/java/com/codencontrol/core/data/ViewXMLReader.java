package com.codencontrol.core.data;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.codencontrol.ui.dialog.PopUp;

import javafx.scene.control.Alert;

public class ViewXMLReader {
	
	private Document doc;
	
	/**
	 * Creates the xml reader with the given input stream.
	 * @param xmlFile
	 */
	public ViewXMLReader(InputStream xmlFile) {
		try {
			
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		    doc = dBuilder.parse(xmlFile);
		    doc.getDocumentElement().normalize();
		    
		} catch (SAXException | IOException | ParserConfigurationException e) {
			PopUp.showPopUp(Alert.AlertType.ERROR, "Unable to read 'views.xml'",
    				"Was unable to read the 'views.xml' file. Maybe it does not exist.");
		}
	}
	
	/**
	 * Returns a NodeList object with all the nodes that match
	 * the tag name.
	 * @param tagName
	 * @return NodeList
	 */
	public NodeList getTagList(String tagName) {
		return doc.getElementsByTagName(tagName);
	}
	
	/**
	 * This function takes in a list of nodes and returns a list
	 * of elements.
	 * @param nodeList
	 * @return List<Element>
	 */
	public List<Element> getElementsFromNodes(NodeList nodeList) {
		List<Element> elementList = new ArrayList<>();
		
		// Loops through all the view nodes
        for (int i = 0; i < nodeList.getLength(); i++) {
        	// Variables
        	Node viewNode = nodeList.item(i);
        	
        	// if its an element node then add it to the list.
        	if (viewNode.getNodeType() == Node.ELEMENT_NODE) {
        		elementList.add((Element) viewNode);
        	}
        }
        
        return elementList;
	}
}