package com.codencontrol.core.util;

import com.registry.RegStringValue;
import com.registry.RegistryKey;

public class RegistryValues {
	
	private String installationPath;
	private String chachePath;
	private String logPath;
	
	// The path to this application in the registry.
	private static String REGISTRY_PATH = "SOFTWARE\\Tenuta\\CodeNControl\\" + Constants.APPLICATION_VERSION;
	private static String REGISTRY_CACHE = "CachePath";
	private static String REGISTRY_LOG = "LogPath";
	private static String REGISTRY_INSTALLATION_PATH = "InstallationPath";
	
	// The singleton of this object.
	private static RegistryValues instance;
	
	/**
	 * This returns the singleton object of registry values.
	 * @return RegistryValues
	 */
	public static RegistryValues getInstance() {
		if (instance == null) {
			instance = new RegistryValues();
		}
		return instance;
	}
	
	/**
	 * This is the constructor for the object that holds all the registry
	 * values for the application.
	 */
	private RegistryValues() {
		loadRegistryValues();
	}
	
	/**
	 * This function goes to the specific registry path that
	 * was created with the installer and loads all the values in
	 * the keys.
	 */
	private void loadRegistryValues() {
		
		RegistryKey applicationKey = null;
		
		// Finds the 'HKEY_LOCAL_MACHINE' Registry Key
		for (RegistryKey key : RegistryKey.listRoots()) {
			
			if (key.toString().equals("HKEY_LOCAL_MACHINE")) {
				applicationKey = new RegistryKey(key, REGISTRY_PATH);
			}
		}
		
		// Loads the data from the cache value.
		RegStringValue regValueCache = (RegStringValue) applicationKey.getValue(REGISTRY_CACHE);
		chachePath = regValueCache.getValue();
		
		// Loads the data from the log value.
		RegStringValue regValueLog = (RegStringValue) applicationKey.getValue(REGISTRY_LOG);
		logPath = regValueLog.getValue();
		
		// Loads the data from the installation path value.
		RegStringValue regValueInstallationPath = (RegStringValue) applicationKey.getValue(REGISTRY_INSTALLATION_PATH);
		installationPath = regValueInstallationPath.getValue();
	}

	/**
	 * Returns the data within the installationPath value within the application key in the registry.
	 * @return String
	 */
	public String getInstallationPath() {
		return installationPath;
	}

	/**
	 * Returns the data within the chachePath value within the application key in the registry.
	 * @return String
	 */
	public String getChachePath() {
		return chachePath;
	}
	
	/**
	 * Returns the data within the logPath value within the registry for this application.
	 * @return String
	 */
	public String getLogPath() {
		return logPath;
	}
}