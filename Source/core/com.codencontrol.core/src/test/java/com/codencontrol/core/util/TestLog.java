package com.codencontrol.core.util;

import java.io.File;
import java.io.PrintStream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.google.common.io.Files;

public class TestLog {
	
	/**
	 * Tests the log file was created
	 * properly and tests that the error and
	 * output streams were redirected.
	 */
	@Test
	public void testRegistryValuesLoaded() {
		
		// Tests log file exists
		File myTempDir = Files.createTempDir();
		Log.getInstance().redirect(myTempDir.toString());
		String logFileName = Log.getInstance().getLogName();
		File logFile = new File(myTempDir.toString() + File.separator + logFileName);
		Assertions.assertTrue(logFile.exists());

		// Tests the strems were redirected.
		PrintStream errorStream = System.err;
		PrintStream outputStream = System.out;
		Assertions.assertEquals(errorStream, outputStream);
	}
}