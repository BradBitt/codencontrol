package com.codencontrol.core.data;

import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class TestViewXMLReader {
	
	/**
	 * Tests the functions of the xml reader.
	 */
	@Test
	public void testXMLReader() {
		
		// Creates the object
		ViewXMLReader xmlReader = new ViewXMLReader(
				this.getClass().getResourceAsStream("/textXML.xml"));
		
		NodeList nodeList = xmlReader.getTagList("view");
		System.out.println(nodeList.getLength());
		Assertions.assertEquals(nodeList.getLength(), 2);
		
		List<Element> elementList = xmlReader.getElementsFromNodes(nodeList);
		Assertions.assertEquals(elementList.size(), 2);
	}
}