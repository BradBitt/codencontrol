package com.codencontrol.ui.toolbar;

import com.codencontrol.ui.util.Constants;

import javafx.scene.control.Menu;

public class MenuBar extends javafx.scene.control.MenuBar {
	
	/**
	 * Creates the tool bar menu bar and styles it.
	 */
	public MenuBar() {	
		// Adds Styling for the toolBarMenuBar
		style();
	}
	
	/**
	 * This function adds styling to the tool bar menu bar.
	 */
	private void style() {
		// Sets the id for the style sheet.
		setId(Constants.TOOL_BAR_MENU_BAR_ID);
	}
	
	/**
	 * This function adds a menu item to the menu.
	 */
	public void addMenuItem(Menu menuToAdd) {
		// Searches for a menu with the same name
		// if it finds one, then adds the menu items to that
		// menu instead of creating a new menu.
		MenuBarAddItemThread thread = new MenuBarAddItemThread(this, menuToAdd);
		thread.start();
	}
}
