package com.codencontrol.ui.listener.tab;

import java.util.ArrayList;

import com.codencontrol.ui.stage.MainStage;
import com.codencontrol.ui.view.MainViewContainer;
import com.codencontrol.ui.view.tab.TabBox;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Bounds;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;

public class TabListener {
	
	private TabBox tabBox;
	private MainViewContainer mainViewContainer;
	private double coordinateX;
	private double coordinateY;
	
	public TabListener(TabBox tabBox) {
		this.tabBox = tabBox;
		
		// the tab box view container
		mainViewContainer = tabBox.getLinkedMainViewContainer();
		
		// Makes the listeners for the tab box.
		makeListeners();
	}
	
	/**
	 * This function creates all the listeners and their functionality.
	 */
	private void makeListeners() {
		tabBox.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                mouseDragged(event);
            }
        });
		tabBox.setOnMouseReleased(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                mouseReleased(event);
            }
        });
	}
	
	/**
	 * This function is called when the mouse is released.
	 * First it checks if the mouse was released in a different container and
	 * then it moves the tab to a different container.
	 * Second it checks if the mouse was released on another tab in the same container
	 * if so then a reordering might occur.
	 * @param event
	 */
    private void mouseReleased(MouseEvent event) {
    	tabBox.setCursor(Cursor.DEFAULT);
    	
    	// Checks move container function
    	moveContainer();
    	
    	// Checks reorder tab function
    	changeOrdering();
    }

    /**
	 * This function is called when the user drags the tab.
	 * This function gets the coordinates of where the mouse
	 * is currently on screen.
	 */
    private void mouseDragged(MouseEvent event) {
    	// Sets the cursor so the user knows they are doing something.
    	tabBox.setCursor(Cursor.HAND);
    	
    	// Sets the current coordinates
    	coordinateX = event.getSceneX();
    	coordinateY = event.getSceneY();
    }
    
    /**
     * This function checks to see if the mouse was released in a different view container
     * and then moves the view over to that new container.
     */
    private void moveContainer() {
    	// First checks to see if the mouse was released within its own container
    	Bounds boundsInSceneOld = mainViewContainer.localToScene(mainViewContainer.getBoundsInLocal());
    	// If this statement is true then its been released in its own container and so nothing happens.
    	if ((coordinateX >= boundsInSceneOld.getMinX() && coordinateX <= boundsInSceneOld.getMaxX())
    			&& (coordinateY >= boundsInSceneOld.getMinY() && coordinateY <= boundsInSceneOld.getMaxY())) {
    		return;
    	}
    	
    	// Loops through all the main containers and checks to see if it was released in any of these view containers.
    	ArrayList<MainViewContainer> allMainViewContainers = ((MainStage) tabBox.getScene().getWindow()).getAllMainViewContainers();
    	for (int i = 0; i < allMainViewContainers.size(); i++) {
    		
    		// Checks to see if the mouse was released within any of these containers
        	Bounds boundsInScene = allMainViewContainers.get(i).localToScene(allMainViewContainers.get(i).getBoundsInLocal());
        	
        	// If this statement is true then the mouse was released within this container
        	if ((coordinateX >= boundsInScene.getMinX() && coordinateX <= boundsInScene.getMaxX())
        			&& (coordinateY >= boundsInScene.getMinY() && coordinateY <= boundsInScene.getMaxY())) {
        		// adds the view to the new container
        		allMainViewContainers.get(i).addView(tabBox.getView());
        		
        		// removes the view from the old container
        		mainViewContainer.removeView(tabBox.getView());
        	}
    	}
    }
    
    /**
     * This function changes the ordering of the tabs inside the current view container.
     */
    private void changeOrdering() {
    	// First checks to see if the mouse was released within its own container
    	Bounds boundsInSceneOfOwn = tabBox.localToScene(tabBox.getBoundsInLocal());
    	// If this statement is true then its been released in its own container and so nothing happens.
    	if ((coordinateX >= boundsInSceneOfOwn.getMinX() && coordinateX <= boundsInSceneOfOwn.getMaxX())
    			&& (coordinateY >= boundsInSceneOfOwn.getMinY() && coordinateY <= boundsInSceneOfOwn.getMaxY())) {
    		return;
    	}
    	
    	// Gets a list of all the tabs in the main view container
    	ObservableList<Node> allTabs = FXCollections.observableArrayList(mainViewContainer.getTabContainer().getChildren());	
    	for (int i = 0; i < allTabs.size(); i++) {
    		// Checks to see if the mouse was released within any of these containers
        	Bounds boundsInScene = allTabs.get(i).localToScene(allTabs.get(i).getBoundsInLocal());
        	
        	// If this statement is true then the mouse was released within this container
        	if ((coordinateX >= boundsInScene.getMinX() && coordinateX <= boundsInScene.getMaxX())
        			&& (coordinateY >= boundsInScene.getMinY() && coordinateY <= boundsInScene.getMaxY())) {
        		
        		// There is a check to see which side of the tab box was the mouse released more on
        		// then applies the ordering to the main view container.
        		if (coordinateX - boundsInScene.getMinX() >= boundsInScene.getMaxX() - coordinateX) {
        			mainViewContainer.changeOrderToRight(tabBox, (TabBox) allTabs.get(i));
        		} else {
        			mainViewContainer.changeOrderToLeft(tabBox, (TabBox) allTabs.get(i));
        		}
        	}
    	}
    }
}