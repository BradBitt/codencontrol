package com.codencontrol.ui.toolbar;

import com.codencontrol.ui.util.Constants;

import javafx.scene.layout.HBox;

public class ToolBar extends HBox {
	
	/**
	 * This is a tool bar button container that contains all the buttons.
	 * Views can add their own buttons to this tool bar.
	 */
	public ToolBar() {
		// Styles this container
		style();
	}
	
	/**
	 * This function adds all the styling to the tool bar button container.
	 */
	private void style() {
		// Sets the id for the style sheet.
		setId(Constants.TOOL_BAR_BUTTON_CONTAINER_ID);
	}
	
	/**
	 * This function adds a button to the tool bar button.
	 * @param ToolBarButton
	 */
	public void addToolBarButton(ToolBarButton button) {
		getChildren().add(button);
	}
}