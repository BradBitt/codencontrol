package com.codencontrol.ui.listener.resize;

import com.codencontrol.ui.view.MainViewContainer;

import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.input.MouseEvent;

/**
 * This class is an abstract class to resize one of the edges of a container.
 * A sub class must be implemented to handle dragging of one of the edges.
 * @author Bradley Tenuta
 */
public abstract class DragResizer {
	
	// The area (pixels) where the user can drag the container
	protected static final int RESIZE_MARGIN = 2;
	
	protected boolean wasClickedInZone = false;
	protected final MainViewContainer mainViewContainer;
	
	/**
	 * Needs a region to apply the resize drag to.
	 * @param region
	 */
	public DragResizer(MainViewContainer mainViewContainer) {
		this.mainViewContainer = mainViewContainer;
	}

	/**
	 * This function creates listeners for dragging the container.
	 * The listeners include on hover, on mouse release and
	 * on mouse drag.
	 */
    public void makeResizable() {
    	mainViewContainer.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                mouseDragged(event);
            }
        });
    	mainViewContainer.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                mouseOver(event);
            }
        });
    	mainViewContainer.setOnMouseReleased(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                mouseReleased(event);
            }
        });
    	mainViewContainer.setOnMousePressed(new EventHandler<MouseEvent>() {
    		@Override
            public void handle(MouseEvent event) {
                mouseClicked(event);
            }
    	});
    }
    
    /**
     * This function is called when the user has stopped dragging.
     * @param event
     */
    protected void mouseReleased(MouseEvent event) {
    	mainViewContainer.setCursor(Cursor.DEFAULT);
    	
    	// Rests the clicked in zone boolean
    	wasClickedInZone = false;
    }
    
    /**
     * Checks if the click occurred within the zone. If it has then the
     * drag function can be applied.
     * @param event
     */
    protected void mouseClicked(MouseEvent event) {
    	if (isInZone(event)) {
    		wasClickedInZone = true;
    	}
    }
    
    /**
     * This function must be implemented by a subclass.
     * This is what happens when the user is dragging with the mouse.
     * @param event
     */
    protected abstract void mouseDragged(MouseEvent event);
    
    /**
     * This function must be implemented by a subclass.
     * This function is what happens when the user is hovering over
     * the container edge.
     * @param event
     */
    protected abstract void mouseOver(MouseEvent event);
    
    /**
     * A boolean check to see if the mouse is above
     * the location where dragging is aloud. This by the 
     * edge where the dragging is to be.
     * @param event
     */
    protected abstract boolean isInZone(MouseEvent event);
}