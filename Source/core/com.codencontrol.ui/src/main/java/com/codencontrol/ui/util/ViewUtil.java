package com.codencontrol.ui.util;

import java.util.ArrayList;
import java.util.List;

import com.codencontrol.ui.factory.GuiFactory;
import com.codencontrol.ui.view.MainView;
import com.codencontrol.ui.view.MainViewContainer;

public class ViewUtil {
	
	/**
	 * This function takes in a view and finds the container
	 * it is currently in.
	 * This function only finds views that are open.
	 * null is returned if no view can be found in any container.
	 * @return MainViewContainer
	 */
	public static MainViewContainer findViewInContainers(MainView view) {
		// Gets a list of all the main view containers.
		ArrayList<MainViewContainer> allContainers = GuiFactory.getInstance().getMainStage().getAllMainViewContainers();
		
		// Loops through all the containers and all the views within the containers for a match.
		for (MainViewContainer Container : allContainers) {
			
			for (MainView heldView : Container.getHeldViews()) {
				
				// Checks the names to see if they are equal
				if (heldView.getName().equals(view.getName())) {
					return Container;
				}
			}
		}
		
		return null;
	}
	
	/**
	 * This function finds a main view container based on the position provided.
	 * This is because each main view container is given a different position.
	 * @param position
	 * @return MainViewContainer
	 */
	public static MainViewContainer findViewContainer(MainViewContainer.ContainerPosition position) {
		// Gets a list of all the main view containers.
		ArrayList<MainViewContainer> allContainers = GuiFactory.getInstance().getMainStage().getAllMainViewContainers();
		
		// Loops through all the containers
		for (MainViewContainer container : allContainers) {
			
			// If the positions match then return that container.
			if (container.getViewPosition() == position ) {
				return container;
			}
		}
		
		// Returns null if there is no matching container.
		return null;
	}
	
	/**
	 * This function returns a list of all the open views in the
	 * application.
	 * @return List<MainView>
	 */
	public static List<MainView> findAllViews() {
		// The List to return
		ArrayList<MainView> views = new ArrayList<>();
		
		// Gets a list of all the main view containers.
		ArrayList<MainViewContainer> allContainers = GuiFactory.getInstance().getMainStage().getAllMainViewContainers();
		
		// Loops through all the containers and all the views within the containers for a match.
		for (MainViewContainer Container : allContainers) {
			
			for (MainView heldView : Container.getHeldViews()) {
				views.add(heldView);
			}
		}
		
		// Returns the list of views.
		return views;
	}
}