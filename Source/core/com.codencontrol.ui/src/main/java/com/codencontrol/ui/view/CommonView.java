package com.codencontrol.ui.view;

import java.util.ArrayList;
import java.util.List;

import com.codencontrol.ui.util.ViewUtil;

import javafx.scene.image.ImageView;

public abstract class CommonView extends MainView {
	
	private String id;
	
	/**
	 * This is a constructor for a common view.
	 * This is used when it is possible to have more than one 
	 * instance of this view showing at one time. e.g. a java file editor view.
	 * The ID param must be unique.
	 * @param viewTitle
	 * @param icon
	 * @param id
	 * @throws Exception 
	 */
	public CommonView(String viewTitle, ImageView icon, String id) throws IllegalArgumentException {
		// Checks the view type is valid before continuing.
		this.icon = icon;
		this.viewTitle = viewTitle;
		this.id = id;
		
		// Validates that the view is unique
		if(!isUnique(id)) {
			throw new IllegalArgumentException("Duplicate View Exception!");
		}
	}
	
	/**
	 * This function checks to see if it can find a 
	 * view with the id given. If it can find a view with the
	 * same id then it is not unique.
	 * @param id
	 * @return boolean
	 */
	private static boolean isUnique(String id) {
		ArrayList<CommonView> allCommonViews = new ArrayList<>();
		
		// Get all open views
		List<MainView> views = ViewUtil.findAllViews();
		
		// Gets all views that are common views.
		for (MainView view : views) {
			if (view instanceof CommonView) {
				allCommonViews.add((CommonView) view);
			}
		}
		
		// Checks to see if the given id matches any of the open views
		for (CommonView commonView : allCommonViews) {
			if (commonView.getCommonViewId().equals(id)) {
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * This function returns the ID of the common view.
	 * @return String
	 */
	public String getCommonViewId() {
		return id;
	}
}