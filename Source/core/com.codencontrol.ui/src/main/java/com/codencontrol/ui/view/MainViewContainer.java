package com.codencontrol.ui.view;

import java.util.ArrayList;

import com.codencontrol.ui.util.Constants;
import com.codencontrol.ui.view.action.ViewMenuActions;
import com.codencontrol.ui.view.tab.TabBox;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;

public class MainViewContainer extends BorderPane {
	
	// Utility
	private ArrayList<MainView> heldViews = new ArrayList<>();
	private TabBox currentView = null;
	
	// GUI Elements
	private ViewMenuActions viewMenuActions;
	private MainViewContainer thisMainViewContainer;
	
	protected HBox viewTabs;
	protected Pane viewPane;
	
	// Container Position.
	protected ContainerPosition containerPosition;
	public static enum ContainerPosition {
		CENTRE,
		BOTTOM,
		LEFT,
		Right,
		TOP
	}
	
	public MainViewContainer(ContainerPosition containerPosition) {
		thisMainViewContainer = this;
		this.containerPosition = containerPosition;

		// Makes the tab section of the view container.
		// This is if someone wants to have multiple views in one view container.
		makeActionBarContainer();
		
		// Makes the container where the view will go.
		makeViewGUIContainer();
		
		// Styles the main view container
		styleMainViewContainer();
	}
	
	/**
	 * Makes the GUI for the action bar. This is where the menu items for the
	 * currently visible view will be displayed. As well as all the tabs of all
	 * the views in this view container.
	 */
	private void makeActionBarContainer() {
		// Creates the action bar.
		HBox actionBarContainer = new HBox();
		actionBarContainer.setId(Constants.ACTION_BAR_ID);
		
		// Creates the container that holds all the tabs.
		viewTabs = new HBox();
		
		// Creates the spacer, this creates a gap in the middle of the action bar.
		Pane spacer = new Pane();
		spacer.setId(Constants.ACTION_BAR_SPACER_ID);
	    HBox.setHgrow(spacer, Priority.ALWAYS);
	    
	    // Makes the container for the actions of the current visible view.
	    viewMenuActions = new ViewMenuActions(this);
	    
	    // Adds the elements to its parents.
	    actionBarContainer.getChildren().addAll(viewTabs, spacer, viewMenuActions);
	    setTop(actionBarContainer);
	}
	
	/**
	 * This makes the GUI where the the view GUI will be held.
	 */
	private void makeViewGUIContainer() {
		viewPane = new Pane();
		viewPane.setId(Constants.MAIN_VIEW_CONTAINER_VIEW_PANE_ID);
		setCenter(viewPane);
	}
		
	/**
	 * This function adds all the styling to the main view container.
	 */
	private void styleMainViewContainer() {
		// Sets the id for the style sheet.
		setId(Constants.MAIN_VIEW_CONTAINER_ID);
		
		// Adds an on click listener that styles the main view container on click.
		setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				thisMainViewContainer.requestFocus();
			}
		});
	}
	
	/**
	 * This function is used to check that the view being displayed and the tabs 
	 * are in sync. This checks that if there is a tab in the main view container that
	 * at one is always being shown.
	 */
	private void refresh() {
		// If there is a current view and its in the list then do nothing
		if (currentView != null && heldViews.contains(currentView.getView())) {
			return;
		}
				
		// If no views then do nothing
		if (currentView == null && heldViews.isEmpty()) {
			viewPane.getChildren().clear();
			return;
		}
		
		// If there is a current view but no views in the main view container
		if (currentView != null && heldViews.isEmpty()) {
			viewPane.getChildren().clear();
			currentView = null;
			return;
		}
		
		// If there is a current view but not in the list
		if (currentView != null && !heldViews.contains(currentView.getView())) {
			// Sets the new current view
			currentView = (TabBox) viewTabs.getChildren().get(0);
			// Adds its view to the pane.
			viewPane.getChildren().add(currentView.getView());
			// Styles the tab.
			currentView.setId(Constants.SELECTED_TAB_BOX_ID);
			return;
		}
		
		// If no current view and there is views then set first view as current view
		if (currentView == null && !heldViews.isEmpty()) {
			// Sets the new current view
			currentView = (TabBox) viewTabs.getChildren().get(0);
			// Adds its view to the pane.
			viewPane.getChildren().add(currentView.getView());
			// Styles the tab.
			currentView.setId(Constants.SELECTED_TAB_BOX_ID);
			return;
		}
	}
	
	/**
	 * This function takes a set of buttons from a view.
	 * It then uses those buttons as the main view container action menu.
	 * @param actionButtons
	 */
	public void setViewMenuActions(Button[] actionButtons) {
		viewMenuActions.getViewActions().setAll(actionButtons);
	}
	
	/**
	 * Adds a view to the main view container.
	 * @param view
	 */
	public void addView(MainView view) {
		// Adds the view to the list of views.
		heldViews.add(view);
		
		// Creates the tab the view will be linked to.
		TabBox tab = new TabBox(view, this);
		
		// Add tab to view tab container.
		viewTabs.getChildren().add(tab);
		
		// refreshes the main view container.
		refresh();
		
		// Updates the view properties
		view.setPreviousPosition(this.containerPosition);
	}
	
	/**
	 * This function removes a view and tab from the main view container.
	 * @param view
	 */
	public void removeView(MainView view) {
		// removes the view that matches the same name. Names are unique for views.
		for (int i = 0; i < heldViews.size(); i++) {
			MainView otherView = (MainView) heldViews.get(i);
			if (otherView.getName().equals(view.getName())) {
				viewTabs.getChildren().remove(i);
				heldViews.remove(i);
			}
		}
		
		// refreshes the main view container.
		refresh();
	}
	
	/**
	 * This function changes the current view in the main view container.
	 * @param tab
	 */
	public void changeView(TabBox tab) {
		// Removes the style class from the old current view.
		currentView.setId(Constants.TAB_BOX_ID);
		viewPane.getChildren().clear();
		
		// Adds the style to the new current tab, and its view to the pane.
		tab.setId(Constants.SELECTED_TAB_BOX_ID);
		viewPane.getChildren().add(tab.getView());
		currentView = tab;
	}
	
	/**
	 * This takes in two tabs, one to be moved to the left of the other.
	 * @param tabBeingMoved
	 * @param tabReleasedOn
	 */
	public void changeOrderToRight(TabBox tabBeingMoved, TabBox tabReleasedOn) {
		// Gets a list of all tabs
		ObservableList<Node> allTabs = FXCollections.observableArrayList(viewTabs.getChildren());
		
		// Loops through the tabs, find the target tab that was released on
		// and adds the tab being moved beside it.
		for(int i = 0; i < allTabs.size(); i++) {
			if (allTabs.get(i) == tabReleasedOn) {
				allTabs.remove(tabBeingMoved);
				allTabs.add(i,tabBeingMoved);
			}
		}
		
		// Sets the new ordering of the tabs.
		viewTabs.getChildren().setAll(allTabs);
	}
	
	/**
	 * This takes in two tabs, one to be moved to the right of the other.
	 * @param tabBeingMoved
	 * @param tabReleasedOn
	 */
	public void changeOrderToLeft(TabBox tabBeingMoved, TabBox tabReleasedOn) {
		// Gets a list of all tabs
		ObservableList<Node> allTabs = FXCollections.observableArrayList(viewTabs.getChildren());
		
		// Loops through the tabs, find the target tab that was released on
		// and adds the tab being moved beside it.
		for(int i = 0; i < allTabs.size(); i++) {
			if (allTabs.get(i) == tabReleasedOn) {
				allTabs.remove(tabBeingMoved);
				if (i == 0) {
					allTabs.add(i,tabBeingMoved);
				} else {
					allTabs.add(i-1,tabBeingMoved);
				}
			}
		}
		
		// Sets the new ordering of the tabs.
		viewTabs.getChildren().setAll(allTabs);
	}
	
	/**
	 * This function returns all the views being held by this view container.
	 * @return ArrayList<MainView>
	 */
	public ArrayList<MainView> getHeldViews() {
		return heldViews;
	}

	/**
	 * This function returns the container of the tabs
	 * @return ObservableList<Node>
	 */
	public HBox getTabContainer() {
		return viewTabs;
	}

	/**
	 * This function returns the main pane that holds the view.
	 * @return Pane
	 */
	public Pane getViewPane() {
		return viewPane;
	}
	
	/**
	 * This is returns the container position.
	 * @return ContainerPosition
	 */
	public ContainerPosition getViewPosition() {
		return containerPosition;
	}
}