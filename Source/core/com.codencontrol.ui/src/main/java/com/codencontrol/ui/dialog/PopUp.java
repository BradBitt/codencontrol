package com.codencontrol.ui.dialog;

import java.io.PrintWriter;
import java.io.StringWriter;

import com.codencontrol.ui.util.Constants;

import javafx.scene.control.Alert;
import javafx.scene.control.DialogPane;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.Stage;

public class PopUp {
	
	/**
	 * Causes a pop up to show on screen with a message.
	 * The alert type is the type of alert the pop up is.
	 * @param alertType
	 * @param header
	 * @param content
	 */
	public static void showPopUp(Alert.AlertType alertType, String header, String content) {
		Alert alert = new Alert(alertType);

		// Styles the alert dialog box
		style(alert);
		
		// Sets up and opens the alert
		alert.setHeaderText(header);
		alert.setContentText(content);
		alert.showAndWait();
	}
	
	/**
	 * This function creates a pop up and displays it.
	 * This pop up comes with an exception box though, explaining what the problem was.
	 * @param alertType
	 * @param header
	 * @param content
	 * @param exception
	 */
	public static void showPopUpWithException(Alert.AlertType alertType, String header, String content, Exception exception) {
		Alert alert = new Alert(alertType);
		
		// Prints the exception
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		exception.printStackTrace(pw);
		String exceptionText = sw.toString();
		
		// Also prints the exception to log file.
		exception.printStackTrace();

		TextArea textArea = new TextArea(exceptionText);
		textArea.setEditable(false);

		// Sets styles
		textArea.setMaxWidth(Double.MAX_VALUE);
		textArea.setMaxHeight(Double.MAX_VALUE);
		GridPane.setVgrow(textArea, Priority.ALWAYS);
		GridPane.setHgrow(textArea, Priority.ALWAYS);

		GridPane expContent = new GridPane();
		expContent.setMaxWidth(Double.MAX_VALUE);
		expContent.add(textArea, 0, 1);
		expContent.setId(Constants.POP_UP_GRID_PANE_ID);

		// Set expandable Exception into the dialog pane.
		alert.getDialogPane().setExpandableContent(expContent);
		
		// Styles the alert dialog box
		style(alert);

		// Sets up and opens the alert
		alert.setHeaderText(header);
		alert.setContentText(content);
		alert.showAndWait();
	}
	
	/**
	 * Styles the alert window.
	 * @param alert
	 */
	private static void style(Alert alert) {
		// Sets the alert style sheet
		DialogPane dialogPane = alert.getDialogPane();
		dialogPane.getStylesheets().add(
				PopUp.class.getResource(Constants.STYLE_SHEET_NAME).toExternalForm());
		dialogPane.setId(Constants.POP_UP_ID);
		
		// Sets the window icon.
		((Stage)alert.getDialogPane().getScene().getWindow()).getIcons().add(
				new Image(PopUp.class.getResourceAsStream(Constants.LOGO_IMAGE_64)));
	}
}