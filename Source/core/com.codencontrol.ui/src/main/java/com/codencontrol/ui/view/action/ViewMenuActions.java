package com.codencontrol.ui.view.action;

import com.codencontrol.ui.util.Constants;
import com.codencontrol.ui.view.MainViewContainer;

import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.layout.HBox;

public class ViewMenuActions extends HBox {
	
	private HBox viewActionContainer;
	private HBox viewContainerActionContainer;

	public ViewMenuActions(MainViewContainer mainViewContainer) {
		// Makes the HBox that the view actions go in
		viewActionContainer = new HBox();
		
		// Make default buttons
		viewContainerActionContainer = new HBox();
		addDefaultButtons(mainViewContainer);
		
		// Styles the container
		style();
		
		// Adds the smaller containers to the main container
		getChildren().addAll(viewActionContainer, viewContainerActionContainer);
	}
	
	/**
	 * This function adds all the default buttons to the view menu container.
	 * It also creates the functionality for the view menu container.
	 */
	private void addDefaultButtons(MainViewContainer mainViewContainer) {
		// Adds all the default buttons to the default button container.
		viewContainerActionContainer.getChildren().addAll();
	}
	
	/**
	 * This function styles the container of the view menu actions.
	 */
	private void style() {
		setId(Constants.ACTION_BAR_MENU_ITEMS_ID);
	}

	/**
	 * This function returns an array of all the view buttons
	 * for that specific view.
	 * @return ObservableList<Node>
	 */
	public ObservableList<Node> getViewActions() {
		return viewActionContainer.getChildren();
	}
	
	/**
	 * This function returns an array list of all the main view container default
	 * buttons. 
	 * @return ObservableList<Node>
	 */
	public ObservableList<Node> getMainViewContainerActions() {
		return viewContainerActionContainer.getChildren();
	}
}