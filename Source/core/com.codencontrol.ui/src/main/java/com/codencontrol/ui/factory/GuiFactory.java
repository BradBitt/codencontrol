package com.codencontrol.ui.factory;

import java.util.ArrayList;

import com.codencontrol.ui.util.Constants;
import com.codencontrol.ui.stage.MainStage;
import com.codencontrol.ui.view.MainView;
import com.codencontrol.ui.view.UniqueView;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * This class is in charge of making the GUI for the application.
 * @author Bradley
 */
public class GuiFactory {
	
	private static GuiFactory instance;
	private MainStage mainStage;
	private ViewFactory viewFactory;
	
	/**
	 * Creates an instance of the GUI factory.
	 * @return GuiFactory
	 */
	public static GuiFactory getInstance() {
		if (instance == null) {
			instance = new GuiFactory();
		}
		return instance;
	}
	
	private GuiFactory() {
		viewFactory = new ViewFactory();
	}
	
	/**
	 * Builds the main stage if its not already being shown.
	 */
	public void buildGui() {
		if (mainStage == null) {
			mainStage = new MainStage();
		} 
	}
	
	/**
	 * This function loads the default buttons and menus.
	 */
	public void loadDefaultGui(ArrayList<UniqueView> allUniqueViews) {
		// Creates the file menu
		makeFileMenu();
		
		// Creates the view menu 
		GuiFactory.getInstance().getViewFactory().buildViewMenu(allUniqueViews);
	}
	
	/**
	 * This function makes the file menu
	 * and its default items, it then adds the menu to the application.
	 */
	private void makeFileMenu() {
		Menu fileMenu = new Menu("File");
		MenuItem exitButton = new MenuItem("Exit");
		
		// Makes exitButton icon
		exitButton.setGraphic(new ImageView(
				new Image(MainView.class.getResourceAsStream(Constants.EXIT_BUTTON_ICON))));
		
		// Creates the action for when the menu item is clicked.
		exitButton.setOnAction(new EventHandler<ActionEvent>() {
			
		    @Override
		    public void handle(ActionEvent e) {
		    	Platform.exit();
		    }
		    
		});
		fileMenu.getItems().add(exitButton);
		mainStage.getMainBarContainer().getMenuBar().addMenuItem(fileMenu);
	}
	
	/**
	 * This function shows the GUI of the application
	 */
	public void showGui() {
		mainStage.show();
	}
	
	/**
	 * This function gets the main stage.
	 * @return MainStage
	 */
	public MainStage getMainStage() {
		return mainStage;
	}
	
	/**
	 * This function returns the View factory object.
	 * @return ViewFactory
	 */
	public ViewFactory getViewFactory() {
		return viewFactory;
	}
}