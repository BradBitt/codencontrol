package com.codencontrol.ui.toolbar;

import com.codencontrol.ui.util.Constants;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;

public class ToolBarButton extends Button {
	
	/**
	 * This is the constructor for the Tool bar button that takes an image.
	 * @param graphic
	 */
	public ToolBarButton(ImageView graphic) {
		super();
		
		// Sets the image for the button
		graphic.setFitHeight(Constants.TOOL_BAR_BUTTON_SIZE);
		graphic.setFitWidth(Constants.TOOL_BAR_BUTTON_SIZE);
		setGraphic(graphic);
		
		// Sets the style for the button
		style();
	}
	
	/**
	 * This function adds the styling for the tool bar button.
	 */
	private void style() {
		getStyleClass().removeAll(getStyleClass());
		setId(Constants.TOOL_BAR_BUTTON_ID);
	}
}