package com.codencontrol.ui.toolbar;

import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;

public class MenuBarAddItemThread extends Thread {
	
	private MenuBar menuBar;
	private Menu menuToAdd;
	
	/**
	 * Constructor for the menu thread.
	 * @param menuBar
	 * @param menuToAdd
	 */
	public MenuBarAddItemThread(MenuBar menuBar, Menu menuToAdd) {
		this.menuBar = menuBar;
		this.menuToAdd = menuToAdd;
	}
	
	/**
	 * This function is run in a separate thread.
	 * It checks that the menu being added exists. If it does
	 * exists then it just adds the menu to the existing menu,
	 * it does this in a separate thread to free up the UI thread.
	 */
	@Override
	public void run() {
		ObservableList<Menu> allCurrentMenus = menuBar.getMenus();
		
		// Loops through all the menu's for a name match.
		for (Menu aCurrentMenu : allCurrentMenus) {
			
			// If it finds a match then add the items to the existing menu
			if (aCurrentMenu.getText().equals(menuToAdd.getText())) {
				for (MenuItem menuToAddItem : menuToAdd.getItems()) {
					
					// Adds the menu item later
					Platform.runLater(new Runnable() {
						@Override
						public void run() {
							aCurrentMenu.getItems().add(menuToAddItem);
						}
					});
					
				}
				return;
			}
		}
		
		// If menu name is not found then just add the menu.
		menuBar.getMenus().add(menuToAdd);
	}

}
