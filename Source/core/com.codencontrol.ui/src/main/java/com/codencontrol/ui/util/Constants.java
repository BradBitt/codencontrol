package com.codencontrol.ui.util;

public final class Constants {
	
	public static String TAB_BOX_ID = "tabBox";
	public static String TAB_BOX_CLOSE_BUTTON_ID = "tabBoxCloseButton";
	public static String SELECTED_TAB_BOX_ID = "selectedTab";
	public static String ACTION_BAR_ID = "actionBarContainer";
	public static String ACTION_BAR_SPACER_ID = "viewActionBarSpacer";
	public static String ACTION_BAR_MENU_ITEMS_ID = "viewMenuActionsContainer";
	public static String MAIN_VIEW_CONTAINER_ID = "mainViewContainer";
	public static String MAIN_VIEW_CONTAINER_VIEW_PANE_ID = "viewPane";
	public static String PROGRAM_NAME = "Code'n'Control";
	public static String TOOL_BAR_BUTTON_CONTAINER_ID = "toolBarButtonContainer";
	public static String TOOL_BAR_BUTTON_ID = "toolBarButton";
	public static String TOOL_BAR_MENU_BAR_ID = "toolBarMenuBar";
	public static String POP_UP_ID = "popUp";
	public static String POP_UP_GRID_PANE_ID = "popUpGridPane";
	
	// Sizes
	public static int TOOL_BAR_BUTTON_SIZE = 20;
	public static int MAIN_VIEW_CONTAINER_BOTTOM_PREF_HEIGHT = 250;
	public static int MAIN_VIEW_CONTAINER_LEFT_PREF_WIDTH = 400;
	public static double MAIN_VIEW_CONTAINER_BIND_PROPERTY = 1.2;
	
	// Style sheets
	public static String STYLE_SHEET_NAME = "/UiStyling.css";
	
	// Images
	public static String TAB_CLOSE_ICON = "/close_icon.png";
	public static String EXIT_BUTTON_ICON = "/exit_button_icon.png";
	public static String TAB_HIGHLIGHTED_CLOSE_ICON = "/close_icon_hover.png";
	public static String VIEW_ACTION_HIDE_ICON = "/hide_button.png";
	public static String VIEW_ACTION_SHOW_ACTION = "/show_button.png";
	
	// LOGO images
	public static String LOGO_IMAGE_32 = "/Logo_gradianet1_v2_32x32.png";
	public static String LOGO_IMAGE_64 = "/Logo_gradianet1_v2_64x64.png";
	public static String LOGO_IMAGE_125 = "/Logo_gradianet1_v2_125x125.png";
	public static String LOGO_IMAGE_250 = "/Logo_gradianet1_v2_250x250.png";
	public static String LOGO_IMAGE_500 = "/Logo_gradianet1_v2_500x500.png";
	public static String LOGO_IMAGE_1000 = "/Logo_gradianet1_v2_1000x1000.png";
	
	// The image file names that go along with those view type.
	public static String UTILITY_TYPE_ICON_FILE_NAME = "/utility.png";
	public static String JAVA_TYPE_ICON_FILE_NAME = "/java.png";
	
	private Constants() {}
}