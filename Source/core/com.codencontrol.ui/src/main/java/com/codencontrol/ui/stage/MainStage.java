package com.codencontrol.ui.stage;

import java.util.ArrayList;

import com.codencontrol.ui.listener.resize.DragResizerEast;
import com.codencontrol.ui.listener.resize.DragResizerNorth;
import com.codencontrol.ui.toolbar.MainBarContainer;
import com.codencontrol.ui.util.Constants;
import com.codencontrol.ui.view.MainView;
import com.codencontrol.ui.view.MainViewContainer;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class MainStage extends Stage {
	
	// GUI Elements
	private BorderPane mainStageContainer;
	private BorderPane workspaceContainer;
	private Scene mainStageScene;
	private ArrayList<MainViewContainer> allMainViewContainers = new ArrayList<>();
	
	private MainBarContainer mainBarContainer;
	private MainViewContainer centreMainContainer;
	private MainViewContainer leftMainContainer;
	private MainViewContainer bottomMainContainer;
	
	public MainStage() {
		// Makes the main container of the stage.
		mainStageContainer = new BorderPane();
		
		// Makes the UI Elements that are in the main stage container
		makeMainStageUI();
		
		// Makes the UI elements that are in the workspace container
		workspaceContainer = new BorderPane();
		mainStageContainer.setCenter(workspaceContainer);
		makeWorkspaceUI();
		
		// Sets the scene
		mainStageScene = new Scene(mainStageContainer, 1250, 750);
		setScene(mainStageScene);
		
		// Main Stage Styling.
		styleStage();
	}
	
	/**
	 * This makes the UI that goes into the first border-pane.
	 */
	private void makeMainStageUI() {
		// Makes the toolBar.
		mainBarContainer = new MainBarContainer();
		mainStageContainer.setTop(mainBarContainer);
		
		// Makes the default left hand side view.
		leftMainContainer = new MainViewContainer(MainViewContainer.ContainerPosition.LEFT);
		mainStageContainer.setLeft(leftMainContainer);
		new DragResizerEast(leftMainContainer).makeResizable();
		leftMainContainer.maxWidthProperty().bind(mainStageContainer.widthProperty().divide(Constants.MAIN_VIEW_CONTAINER_BIND_PROPERTY));
		BorderPane.setMargin(leftMainContainer, new Insets(0,1,0,0));
		allMainViewContainers.add(leftMainContainer);
		leftMainContainer.setPrefWidth(Constants.MAIN_VIEW_CONTAINER_LEFT_PREF_WIDTH);
	}
	
	/**
	 * This function makes the UI that goes into the second border-pane.
	 */
	private void makeWorkspaceUI() {
		// Makes the default centre view.
		centreMainContainer = new MainViewContainer(MainViewContainer.ContainerPosition.CENTRE);
		workspaceContainer.setCenter(centreMainContainer);
		BorderPane.setMargin(centreMainContainer, new Insets(0,0,1,1));
		allMainViewContainers.add(centreMainContainer);
		
		// Makes the default bottom view.
		bottomMainContainer = new MainViewContainer(MainViewContainer.ContainerPosition.BOTTOM);
		workspaceContainer.setBottom(bottomMainContainer);
		new DragResizerNorth(bottomMainContainer).makeResizable();
		bottomMainContainer.maxHeightProperty().bind(mainStageContainer.heightProperty().divide(Constants.MAIN_VIEW_CONTAINER_BIND_PROPERTY));
		BorderPane.setMargin(bottomMainContainer, new Insets(1,0,0,1));
		allMainViewContainers.add(bottomMainContainer);
		bottomMainContainer.setPrefHeight(Constants.MAIN_VIEW_CONTAINER_BOTTOM_PREF_HEIGHT);
	}
	
	/**
	 * This function sets the styling for the main stage.
	 */
	private void styleStage() {
		// Sets the name of the stage
		setTitle(Constants.PROGRAM_NAME);
		
		// Adds the Icon
		getIcons().add(new Image(getClass().getResourceAsStream(Constants.LOGO_IMAGE_64)));
		
		// Makes the styling for the UI elements
		mainStageScene.getStylesheets().add(getClass().getResource(Constants.STYLE_SHEET_NAME).toExternalForm());
	}

	/**
	 * This function takes a unique view and adds it to the main view container.
	 * @param view
	 */
	public void addViewToCentreViewContainer(MainView view) {
		// Adds the view to this main view container
		centreMainContainer.addView(view);
	}

	/**
	 * This function takes a unique view and adds it to the main view container.
	 * @param view
	 */
	public void addViewToLeftViewContainer(MainView view) {
		// Adds the view to this main view container
		leftMainContainer.addView(view);
	}

	/**
	 * This function takes a unique view and adds it to the main view container.
	 * @param view
	 */
	public void addViewToBottomViewContainer(MainView view) {
		// Adds the view to this main view container
		bottomMainContainer.addView(view);
	}

	/**
	 * This function returns the main tool bar container.
	 * @return MainToolBarContainer
	 */
	public MainBarContainer getMainBarContainer() {
		return mainBarContainer;
	}

	/**
	 * This function returns an Array List of all the main view containers.
	 * @return ArrayList<MainViewContainer>
	 */
	public ArrayList<MainViewContainer> getAllMainViewContainers() {
		return allMainViewContainers;
	}
	
	/**
	 * This function returns the main stage scene use for this application.
	 * @return Scene
	 */
	public Scene getMainScene() {
		return mainStageScene;
	}
}