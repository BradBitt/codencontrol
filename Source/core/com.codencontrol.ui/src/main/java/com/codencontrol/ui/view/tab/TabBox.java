package com.codencontrol.ui.view.tab;

import com.codencontrol.ui.listener.tab.TabListener;
import com.codencontrol.ui.util.Constants;
import com.codencontrol.ui.view.MainView;
import com.codencontrol.ui.view.MainViewContainer;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;

public class TabBox extends HBox {
	
	private MainView linkedView;
	private MainViewContainer LinkedMainViewContainer;
	private TabBox thisTabBox;
	
	/**
	 * Creates an instance of a TabBox.
	 * A TabBox is an HBox object that has an icon and a title.
	 * Both of these items come from the linked view.
	 * The tab gets linked to the viewPane which allows it to display its linked view
	 * when clicked.
	 * @param linkedView
	 * @param viewPane
	 */
	public TabBox(MainView linkedView, MainViewContainer LinkedMainViewContainer) {
		super();
		
		thisTabBox = this;
		this.linkedView = linkedView;
		this.LinkedMainViewContainer = LinkedMainViewContainer;
		
		// Creates the GUI
		createTabGUI();
		
		// Create on click listener
		createOnClickListener();
		
		// Creates a tab listener
		new TabListener(this);
		
		// Sets the style of the tab box.
		setId(Constants.TAB_BOX_ID);
	}
	
	/**
	 * Creates the GUI for the tab
	 */
	private void createTabGUI() {
		// Sets the icon.
		ImageView icon = linkedView.getIcon();
		
		// Sets the name of the view in the tab.
		Label tabTitle = new Label(linkedView.getName());
		HBox.setMargin(tabTitle, new Insets(0, 5, 0, 5)); // adds margin left and right
		
		// Creates the close image icon
		Button closeIcon = createCloseButton();
		
		// Adds the icon and the title of the view to the tab.
		getChildren().addAll(icon, tabTitle, closeIcon);
	}
	
	/**
	 * This function creates the close button and all its listeners.
	 * @return ImageView
	 */
	private Button createCloseButton() {
		Button closeIcon = new Button();
		closeIcon.getStyleClass().removeAll(closeIcon.getStyleClass());
		closeIcon.setId(Constants.TAB_BOX_CLOSE_BUTTON_ID);
		closeIcon.setGraphic(new ImageView(new Image(TabBox.class.getResourceAsStream(Constants.TAB_CLOSE_ICON))));
		
		// Creates the mouse click listener
		closeIcon.setOnAction(new EventHandler<ActionEvent>() {
		    @Override public void handle(ActionEvent e) {
		    	LinkedMainViewContainer.removeView(thisTabBox.getView());
		    	
		    	// Updates the view properties
		    	thisTabBox.getView().setViewState(false);
		    	thisTabBox.getView().setPreviousPosition(LinkedMainViewContainer.getViewPosition());
		    	
		    	// Calls the save persistent state function.
		    	thisTabBox.getView().onCloseFunction();
		    }
		});
		
		return closeIcon;
	}
	
	/**
	 * Adds the on click listener to the tab.
	 * This sets its linked view to be displayed in the view pane
	 * of the main view container.
	 */
	private void createOnClickListener() {
		setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				// Changes the current tab and view to this tab and view.
				LinkedMainViewContainer.changeView(thisTabBox);
			}
		});
	}
	
	/**
	 * Gets the linked view.
	 * @return MainView
	 */
	public MainView getView() {
		return linkedView;
	}

	/**
	 * This function returns the tab box linked view container.
	 * @return MainViewContainer
	 */
	public MainViewContainer getLinkedMainViewContainer() {
		return LinkedMainViewContainer;
	}
}