package com.codencontrol.ui.listener.resize;

import com.codencontrol.ui.view.MainViewContainer;

import javafx.scene.Cursor;
import javafx.scene.input.MouseEvent;

/**
 * This function is to allow the container to be dragged
 * on its east side.
 * @author Bradley Tenuta
 */
public class DragResizerEast extends DragResizer {

	/**
	 * This is the constructor which follows the implementation
	 * of the super class.
	 * @param mainViewContainer
	 */
	public DragResizerEast(MainViewContainer mainViewContainer) {
		super(mainViewContainer);
	}

	/**
	 * This function is called when the mouse is hovered
	 * over the container. It changes the courser if the mouse
	 * is over the correct zone for dragging.
	 */
	@Override
    protected void mouseOver(MouseEvent event) {
        if (isInZone(event)) {
        	mainViewContainer.setCursor(Cursor.E_RESIZE);
        } else {
        	mainViewContainer.setCursor(Cursor.DEFAULT);
        }
    }
	
	/**
	 * This function sets the new width of the container based
	 * on which direction the dragging is taking place.
	 */
	@Override
    protected void mouseDragged(MouseEvent event) {
		if (wasClickedInZone) {
			mainViewContainer.setPrefWidth(event.getX());
		}
    }

    /**
     * A boolean check to see if the mouse is above
     * the location where dragging is aloud. This by the 
     * edge where the dragging is to be.
     * @param event
     * @return boolean
     */
	@Override
    protected boolean isInZone(MouseEvent event) { 
    	return event.getX() > (mainViewContainer.getWidth() - RESIZE_MARGIN); 
    }
}