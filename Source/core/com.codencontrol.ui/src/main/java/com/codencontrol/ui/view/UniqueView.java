package com.codencontrol.ui.view;

import java.util.HashMap;

public abstract class UniqueView extends MainView {
	
	/**
	 * This is a constructor that is to be used if the view being added is unique.
	 * @param viewType
	 * @param viewTitle
	 */
	public UniqueView(String viewTitle, MainViewContainer.ContainerPosition defaultViewPosition) {
		this.viewTitle = viewTitle;
		this.defaultViewPosition = defaultViewPosition;
		
		// Sets the icon for the view.
		setIcon();
		
		// Creates the view data map.
		setViewMapData(new HashMap<String, String>());
	}
}