package com.codencontrol.ui.view;

import java.util.Map;

import com.codencontrol.ui.util.Constants;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;

public abstract class MainView extends Pane {

	protected String viewTitle;
	protected Map<String, String> mapData; // String map of persistent data.
	protected MainViewContainer.ContainerPosition defaultViewPosition;
	
	protected MainViewContainer.ContainerPosition previousViewPosition = null;
	protected Boolean viewState = false; // States if the view is currently open or closed.
	protected ImageView icon;

	/**
	 * Gets the icon that the view holds.
	 * @return Icon
	 */
	public ImageView getIcon() {
		return icon;
	}
	
	/**
	 * Gets the title of the view.
	 * @return String
	 */
	public String getName() {
		return viewTitle;
	}

	/**
	 * Returns the view map data. This map will contain data that the developer wants the view
	 * to save persistently.
	 * @return Map<String, String>
	 */
	public Map<String, String> getViewMapData() {
		return mapData;
	}
	
	/**
	 * Returns the views previous location.
	 * @return MainViewContainer.ContainerPosition
	 */
	public MainViewContainer.ContainerPosition getPreviousPosition() {
		return previousViewPosition;
	}
	
	/**
	 * Returns the default view position for this view.
	 * @return MainViewContainer.ContainerPosition
	 */
	public MainViewContainer.ContainerPosition getDefaultViewPosition() {
		return defaultViewPosition;
	}
	
	/**
	 * This function sets the icon for the view.
	 * This function should be overwritten if the developer wants
	 * to use their own image for the view icon.
	 */
	public void setIcon() {
		this.icon = new ImageView(
				new Image(MainView.class.getResourceAsStream(Constants.UTILITY_TYPE_ICON_FILE_NAME)));
	}
	
	/**
	 * This sets the view map data. Mainly used when loading the view.
	 * @param mapData
	 */
	public void setViewMapData(Map<String, String> mapData) {
		this.mapData = mapData;
	}

	/**
	 * Set the current state of the view.
	 * @param viewState
	 */
	public void setViewState(Boolean viewState) {
		this.viewState = viewState;
	}
	
	/**
	 * Sets the views previous position.
	 * @param viewPreviousPosition
	 */
	public void setPreviousPosition(MainViewContainer.ContainerPosition previousViewPosition) {
		this.previousViewPosition = previousViewPosition;
	}
	
	/**
	 * Returns the current state of the view, if it is currently open or not
	 * in the application.
	 * @return StateType
	 */
	public Boolean isViewVisible() {
		return viewState;
	}

	/**
	 * This function is called and runs any last minute code that the developer
	 * of the view may want to run before the application is closed. Or before the view is closed
	 * from when the user clicks the close button on that view.
	 */
	public void onCloseFunction() {
		saveViewState();
		removeViewUI();
	}
	
	/**
	 * This function removes the UI when the view is closed.
	 * This is so the run function is safe to call again on view open.
	 */
	public void removeViewUI() {
		this.getChildren().clear();
	}
	
	/**
	 * This function is written by the developer.
	 * This is called before the view is closed.
	 * It allows the developer to save any information to the mapData map
	 * to be saved by the application.
	 */
	public abstract void saveViewState();
	
	/**
	 * This function is called when the view is active.
	 * This function gets called when you open this view from the view menu.
	 * This function is called when the view is loaded if it was left open after previous
	 * session.
	 */
	public abstract void run();
	
	/**
	 * This function adds styling so this view will match the width
	 * and height of its parent.
	 */
	protected void addWidthAndHeightStyling() {
		// Adds styles for this pane.
		Pane parent = (Pane) this.getParent();
		this.prefWidthProperty().bind(parent.widthProperty());
		this.prefHeightProperty().bind(parent.heightProperty());
	}
}