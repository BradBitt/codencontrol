package com.codencontrol.ui.toolbar;

import javafx.scene.layout.VBox;

public class MainBarContainer extends VBox {
	
	private ToolBar toolBar;
	private MenuBar menuBar;
	private ProgressBar progressBar;
	
	/**
	 * This function holds the tool bar menu bar and the
	 * tool bar menu buttons. This container creates those GUI items
	 * and adds it to itself.
	 */
	public MainBarContainer() {	
		// Makes the tool bar menu
		menuBar = new MenuBar();
		
		// Makes the tool bar buttons container
		toolBar = new ToolBar();
		
		// Makes the loading bar
		progressBar = ProgressBar.getInstance();
		// Sets the width of the progress bar to match the parent
		progressBar.prefWidthProperty().bind(widthProperty());
		
		// Adds them to the container
		getChildren().addAll(menuBar, toolBar, progressBar);
	}

	/**
	 * This function gets the tool bar button container.
	 * @return toolBar
	 */
	public ToolBar getToolBar() {
		return toolBar;
	}

	/**
	 * This function gets the tool bar menu bar.
	 * @return menuBar
	 */
	public MenuBar getMenuBar() {
		return menuBar;
	}

	/**
	 * This function gets the progress bar from the tool bar.
	 * @return ToolBarProgressBar
	 */
	public ProgressBar getProgressBar() {
		return progressBar;
	}
}