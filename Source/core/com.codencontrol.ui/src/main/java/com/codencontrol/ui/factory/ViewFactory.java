package com.codencontrol.ui.factory;

import java.util.ArrayList;

import com.codencontrol.ui.dialog.PopUp;
import com.codencontrol.ui.toolbar.ProgressBar;
import com.codencontrol.ui.util.ViewUtil;
import com.codencontrol.ui.view.MainViewContainer;
import com.codencontrol.ui.view.UniqueView;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.image.ImageView;

public class ViewFactory {
	
	/**
	 * This function adds all the views to the
	 * views menu item. This allows a way to open the views
	 * if the view you want is currently not open.
	 * @param allUniqueViews
	 */
	public void buildViewMenu(ArrayList<UniqueView> allUniqueViews) {
		// Creates the menu
		Menu viewMenu = new Menu("Views");
		
		// Loops through all the views and creates the menu actions and menu items.
		for (UniqueView view : allUniqueViews) {
			MenuItem menuItem = new MenuItem(view.getName());
			// Adds view icon to menu item
			ImageView icon = new ImageView(view.getIcon().getImage());
			menuItem.setGraphic(icon);
			
			// Creates the action for when the menu item is clicked.
			menuItem.setOnAction(new EventHandler<ActionEvent>() {
				
			    @Override
			    public void handle(ActionEvent e) {
			    	// Adds the view to the UI.
			    	buildView(view);
			    }
			    
			});
			
			// Adds the menu item to the menu.
			viewMenu.getItems().add(menuItem);
		}
		
		// Adds the menu to the menu bar.
		GuiFactory.getInstance().getMainStage().getMainBarContainer().getMenuBar().addMenuItem(viewMenu);
	}
	
	/**
	 * This function takes a view and adds it to the Ui.
	 * This function uses the progress bar.
	 * @param uniqueView
	 */
	public void buildView(UniqueView uniqueView) {
		// Checks to see if its already open.
		ProgressBar.getInstance().setProgress(0.3);
    	if (ViewUtil.findViewInContainers(uniqueView) != null) {
    		PopUp.showPopUp(Alert.AlertType.INFORMATION, "View Already Open!",
					"Failed to open this view as the view is already open.");
    		return;
    	}
    	
    	// Checks to see if there is a saved preferred position
    	ProgressBar.getInstance().setProgress(0.6);
    	MainViewContainer.ContainerPosition position = null;
    	if (uniqueView.getPreviousPosition() == null) {
    		position = uniqueView.getDefaultViewPosition();
    	} else {
    		position = uniqueView.getPreviousPosition();
    	}
    	
    	// Finds the correct view container and adds the view to it.
    	ProgressBar.getInstance().setProgress(1);
    	MainViewContainer viewContainer = ViewUtil.findViewContainer(position);
    	if (viewContainer == null) {
    		PopUp.showPopUp(Alert.AlertType.ERROR, "Failed To Add View!",
					"Failed to find the main view container to add the view to: "
							+ position.toString());
    	}
    	viewContainer.addView(uniqueView);
    	
    	// Updates the view properties
    	uniqueView.setViewState(true);
    	uniqueView.setPreviousPosition(viewContainer.getViewPosition());
    	
    	// Calls the view run function.
    	uniqueView.run();
    	
    	ProgressBar.getInstance().resetProgress();
	}
}