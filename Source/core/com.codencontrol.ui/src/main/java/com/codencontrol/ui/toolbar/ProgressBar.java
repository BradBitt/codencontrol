package com.codencontrol.ui.toolbar;

public class ProgressBar extends javafx.scene.control.ProgressBar {
	
	private static ProgressBar instance = null;
	
	public static ProgressBar getInstance() {
		if (instance == null) {
			instance = new ProgressBar();
		}
		return instance;
	}
	
	private ProgressBar() {
		super();
		resetProgress();
	}
	
	/**
	 * This resets the progress bar to 0.
	 * This removes the blue bar from the progress bar.
	 */
	public void resetProgress() {
		setProgress(0);
	}
}