package com.codencontrol.cmd.util;

public final class Constants {

	public static String CMD_ICON = "/cmd_icon.png";
	public static String CMD_SYMBOL_ICON = "/cmd_symbol_icon.png";
	public static String CMD_STYLE_SHEET = "/cmd_style_sheet.css";
	
	private Constants() {}
}