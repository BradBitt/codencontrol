package com.codencontrol.cmd;

import com.codencontrol.cmd.handler.OpenTerminalHandler;
import com.codencontrol.cmd.util.Constants;
import com.codencontrol.ui.factory.GuiFactory;
import com.codencontrol.ui.stage.MainStage;
import com.codencontrol.ui.view.MainView;
import com.codencontrol.ui.view.MainViewContainer.ContainerPosition;
import com.codencontrol.ui.view.UniqueView;

import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class TerminalView extends UniqueView {
	
	private OpenTerminalHandler terminalHandler;
	
	/**
	 * Constructor to make the View.
	 * @param viewTitle
	 * @param defaultViewPosition
	 */
	public TerminalView(String viewTitle, ContainerPosition defaultViewPosition) {
		super(viewTitle, defaultViewPosition);
		
		// Makes menu items
		makeMenuItems();
	}
	
	/**
	 * Uses own icon instead of the default one.
	 * Have to override this function in order to do it.
	 */
	@Override
	public void setIcon() {
		this.icon = new ImageView(
				new Image(MainView.class.getResourceAsStream(Constants.CMD_ICON)));
	}

	/**
	 * Function saves values from this view for next time the view
	 * is loaded.
	 */
	@Override
	public void saveViewState() {
		// Does nothing.
	}

	/**
	 * This function gets run when the view is open for the first time.
	 */
	@Override
	public void run() {
		// Adds style sheet
		GuiFactory.getInstance().getMainStage().getMainScene()
			.getStylesheets().add(
					getClass().getResource(Constants.CMD_STYLE_SHEET).toExternalForm());
		
		// Add styling
		addWidthAndHeightStyling();
	}
	
	/**
	 * This function creates the menu items to be added to
	 * the main menu in the application.
	 */
	private void makeMenuItems() {
		// Gets the main stage.
		MainStage mainStage = GuiFactory.getInstance().getMainStage();
		
		// Creates the menu to add
		Menu openTerminalMenu = new Menu("File");
		MenuItem openTerminalMenuItem = new MenuItem("Open Terminal");
		
		// Adds the view icon to the menu item
		openTerminalMenuItem.setGraphic(new ImageView(
				new Image(TerminalView.class.getResourceAsStream(Constants.CMD_ICON))));
		
		terminalHandler = new OpenTerminalHandler(this);
		openTerminalMenuItem.setOnAction(terminalHandler);
		openTerminalMenu.getItems().add(openTerminalMenuItem);
		
		// Adds the button to the main menu bar
		mainStage.getMainBarContainer().getMenuBar().addMenuItem(openTerminalMenu);
	}

	public OpenTerminalHandler getTerminalHandler() {
		return terminalHandler;
	}
}