package com.codencontrol.cmd.handler;

import java.io.File;
import java.io.IOException;
import java.util.List;

import com.codencontrol.cmd.TerminalView;
import com.codencontrol.cmd.thread.ProcessThread;
import com.codencontrol.cmd.util.Constants;
import com.codencontrol.editor.explorer.action.OpenFolderHandler;
import com.codencontrol.ui.util.ViewUtil;
import com.codencontrol.ui.view.MainView;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

// TODO: Needs major clean up.
public class OpenTerminalHandler implements EventHandler<ActionEvent> {
	
	private TerminalView mainView;
	private TextArea terminalTextArea;
	private TextField terminalTextField;
	private File selectedDirectory;
	
	public OpenTerminalHandler(TerminalView mainView) {
		this.mainView = mainView;
	}

	/**
	 * This function is called when the terminal menu item is pressed.
	 * This creates a terminal.
	 */
	@Override
	public void handle(ActionEvent event) {
		// A terminal is only created with the explorer is open
		// and a folder has been selected within the file explorer.
		if (!UpdateFileExplorerPath()) {
			return;
		}
		
		// Removes previous UI elements
		// Clears the current view of its contents.
		mainView.removeViewUI();
		
		// Creates the UI.
		createTerminalUi();
	}
	
	/**
	 * This function checks to see that the file explorer view
	 * is open and that a folder has been selected.
	 * It then sets the selected file and returns true if is successful in doing so.
	 * @return boolean
	 */
	private boolean UpdateFileExplorerPath() {
		List<MainView> allViews = ViewUtil.findAllViews();
		
		// Loops through all the views
		for (MainView view : allViews) {
			// Gets the name of the view and checks it equals the file
			// explorer name.
			if (view.getName().equals(
					com.codencontrol.editor.explorer.util.Constants.FILE_EXPLORER_NAME)) {
				// Casts the view and gets the handler.
				com.codencontrol.editor.explorer.View FileExplorerView =
						(com.codencontrol.editor.explorer.View) view;
				OpenFolderHandler handler = FileExplorerView.getOpenFolderHandler();
				
				// Checks the handler is not null
				if (handler != null) {
					File associatedFile = handler.getAssociatedFile();
					if (associatedFile != null) {
						selectedDirectory = associatedFile;
						return true;
					}
				}
			}
		}
		
		// Returns false if it is not able to find the view and
		// if it is not able to find a selected directory within the
		// file explorer view.
		return false;
	}
	
	/**
	 * This function creates a working terminal and
	 * adds it to the UI.
	 */
	private void createTerminalUi() {
		// Create the VBox container
		VBox verticalBoxContainer = new VBox();
		verticalBoxContainer.prefWidthProperty().bind(mainView.widthProperty());
		verticalBoxContainer.prefHeightProperty().bind(mainView.heightProperty());
		mainView.getChildren().add(verticalBoxContainer);
		
		// Creates the terminal text area
		terminalTextArea = new TextArea();
		terminalTextArea.setId("cmdTextArea");
		terminalTextArea.setEditable(false);
		terminalTextArea.prefWidthProperty().bind(mainView.widthProperty());
		terminalTextArea.prefHeightProperty().bind(mainView.heightProperty().subtract(16));
		terminalTextArea.setOnMouseClicked(new TextAreaClickHandler(this));
		verticalBoxContainer.getChildren().add(terminalTextArea);
		
		// Creates the terminal text field container
		HBox textFieldContainer = new HBox();
		textFieldContainer.prefWidthProperty().bind(mainView.widthProperty());
		verticalBoxContainer.getChildren().add(textFieldContainer);
		
		// Creates the terminal text field symbol
		ImageView terminalTextFiledIcon = new ImageView(
				new Image(this.getClass().getResourceAsStream(Constants.CMD_SYMBOL_ICON)));
		textFieldContainer.getChildren().add(terminalTextFiledIcon);
		
		// Creates the terminal field area
		terminalTextField = new TextField();
		terminalTextField.setId("cmdTextBox");
		terminalTextField.prefWidthProperty().bind(mainView.widthProperty().subtract(16));
		textFieldContainer.getChildren().add(terminalTextField);
		// Text field handler that controls when processes are run.
		terminalTextField.setOnKeyPressed(new TextFieldHandler(this));
	}
	
	/**
	 * This function runs a process.
	 * @param arguments
	 * @throws IOException
	 */
	public void runProcess(String arguments) throws IOException {
		// Creates the process builder.
		// TODO update to actual good file path
		arguments = arguments + " > C:\\Users\\bradl\\Desktop\\Input.txt";
		ProcessBuilder builder = new ProcessBuilder("cmd.exe", "/c", arguments);
		
		// Adds properties to the process builder.
		builder.directory(selectedDirectory);
		builder.redirectErrorStream(true);
		
		// Runs the command and UI update in seperate thread
		ProcessThread thread = new ProcessThread(this, builder);
		thread.start();
	}

	/**
	 * Gets the terminal text area.
	 * @return TextArea
	 */
	public TextArea getTerminalTextArea() {
		return terminalTextArea;
	}

	/**
	 * Gets the terminal text field.
	 * @return TextField
	 */
	public TextField getTerminalTextField() {
		return terminalTextField;
	}
	
	/**
	 * Gets the selected directory.
	 * @return File
	 */
	public File getSelectedDirectory() {
		return selectedDirectory;
	}
}