package com.codencontrol.cmd.handler;

import javafx.event.EventHandler;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

public class TextAreaClickHandler implements EventHandler<MouseEvent> {
	
	private OpenTerminalHandler openTerminalhandler;
	
	public TextAreaClickHandler(OpenTerminalHandler openTerminalhandler) {
		this.openTerminalhandler = openTerminalhandler;
	}

	@Override
	public void handle(MouseEvent arg0) {
		TextField textField = openTerminalhandler.getTerminalTextField();
		textField.requestFocus();
	}
}