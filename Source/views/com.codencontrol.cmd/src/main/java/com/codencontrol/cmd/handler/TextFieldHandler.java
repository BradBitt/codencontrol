package com.codencontrol.cmd.handler;

import java.io.IOException;

import com.codencontrol.ui.dialog.PopUp;

import javafx.event.EventHandler;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public class TextFieldHandler implements EventHandler<KeyEvent> {
	
	private OpenTerminalHandler terminalHandler;
	
	/**
	 * The constructor for the terminal text field handler.
	 * @param terminalHandler
	 */
	public TextFieldHandler(OpenTerminalHandler terminalHandler) {
		this.terminalHandler = terminalHandler;
	}

	/**
	 * This function is called whenever a key is pressed within the
	 * cmd text field.
	 */
	@Override
	public void handle(KeyEvent event) {
		// If the key pressed does not equal enter.
		if (!(event.getCode().equals(KeyCode.ENTER))) {
			return;
		}
		
		// Breaks up the string
		String textFieldString = terminalHandler.getTerminalTextField().getText();

		// Runs the command.
		try {
			terminalHandler.runProcess(textFieldString);
		} catch (IOException e) {
			PopUp.showPopUpWithException(AlertType.ERROR,
					"Failed to read process output",
					"Failed to read the process output. Maybe stream is already closed!",
					e);
		}
		
		// Clears the text field.
		terminalHandler.getTerminalTextField().setText("");
	}
}