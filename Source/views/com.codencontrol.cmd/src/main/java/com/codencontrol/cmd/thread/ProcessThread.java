package com.codencontrol.cmd.thread;

import java.io.File;
import java.io.IOException;
import java.lang.ProcessBuilder.Redirect;
import java.util.Scanner;

import com.codencontrol.cmd.handler.OpenTerminalHandler;
import com.codencontrol.ui.dialog.PopUp;

import javafx.application.Platform;
import javafx.scene.control.Alert.AlertType;

public class ProcessThread extends Thread {
	
	private OpenTerminalHandler terminalHandler;
	private ProcessBuilder processBuilder;
	
	/**
	 * The constructor for the thread.
	 * @param terminalHandler
	 * @param process
	 */
	public ProcessThread(OpenTerminalHandler terminalHandler, ProcessBuilder processBuilder) {
		this.terminalHandler = terminalHandler;
		this.processBuilder = processBuilder;
	}
	
	/**
	 * Gets the output stream of the process and outputs it to the text area.
	 */
	@Override
	public void run() {
		try {
			// TODO update to actual good file path
			File log = new File("C:\\Users\\bradl\\Desktop\\Input.txt");
			processBuilder.redirectOutput(Redirect.appendTo(log));
			
			// Runs the process.
			processBuilder.start();

			// Reads the output stream and prints it to text area.
			Scanner in = new Scanner(log);
			while (in.hasNextLine()) {
				String line = in.nextLine();
				System.out.println(line);
				updateUi(line);
			}
			in.close();

		} catch (IOException e) {
			PopUp.showPopUpWithException(AlertType.ERROR,
					"Failed to read process output",
					"Failed to read the process output. Maybe stream is already closed!",
					e);
		}
	}
	
	/**
	 * Updates the text area UI with the string given.
	 * @param line
	 */
	private void updateUi(String line) {
		Platform.runLater(new Runnable() {
            @Override
            public void run() {
            	terminalHandler.getTerminalTextArea().appendText("\n");
				terminalHandler.getTerminalTextArea().appendText(line);
            }
		});
	}
}