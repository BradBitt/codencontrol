package com.codencontrol.editor.explorer.tree;

import java.io.File;

import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.util.Callback;

public class TreeHolder {

	private TreeView<TreeItemValueHolder> mainTree;
	private File directory;
	
	/**
	 * Creates the tree holder object and creates
	 * the tree based on the directory given.
	 * @param directory
	 */
	public TreeHolder(File directory) {
		this.directory = directory;
		makeTree();
	}
	
	/**
	 * Returns the tree associated with this holder.
	 * @return TreeView<TreeItemHolder>
	 */
	public TreeView<TreeItemValueHolder> getTree() {
		return mainTree;
	}
	
	/**
	 * Returns the directory for this tree.
	 * @return File
	 */
	public File getDirectory() {
		return directory;
	}
	
	/**
	 * This function makes the tree with the directory it is given.
	 * it creates the tree by going through all the sub files and folders
	 * from the directory.
	 */
	private void makeTree() {
		// Makes the tree root item
		TreeItemValueHolder rootItem = new TreeItemValueHolder(directory,
				TreeItemValueHolder.ItemType.FOLDER);
		TreeItem<TreeItemValueHolder> rootTreeItem = new TreeItem<>(rootItem);
	
		// Makes all the sub tree items with recursion.
		makeSubTreeItems(directory, rootTreeItem);
		
		// Adds the items to the tree.
		mainTree = new TreeView<TreeItemValueHolder>(rootTreeItem);
		
		// Sets the cell to be used in the tree.
		mainTree.setCellFactory(new Callback<TreeView<TreeItemValueHolder>,
				TreeCell<TreeItemValueHolder>>(){

			@Override
			public TreeCell<TreeItemValueHolder> call(TreeView<TreeItemValueHolder> treeView) {
				return new CustomTreeCell();
			}
			
		});
	}
	
	/**
	 * This function is recursive and gets all the folders and files
	 * and adds them to the tree view.
	 * @param folder
	 * @param parentItem
	 */
	private void makeSubTreeItems(File folder, TreeItem<TreeItemValueHolder> parentItem) {
		// loops through all the sub files and folders
		// with recursion.
		for (File file : folder.listFiles()) {
			
			// Skips hidden files
			if(file.isHidden()) {
				continue;
			}
			
			// If the file is a directory
			if (file.isDirectory()) {
				// Make folder tree object
				TreeItemValueHolder folderItem = new TreeItemValueHolder(file,
						TreeItemValueHolder.ItemType.FOLDER);
				
				// Add it to the tree
				TreeItem<TreeItemValueHolder> folderTreeItem = new TreeItem<>(folderItem);
				parentItem.getChildren().add(folderTreeItem);
				
				// Then recurse into its children
				makeSubTreeItems(file, folderTreeItem);
			} else {
				TreeItemValueHolder fileItem = new TreeItemValueHolder(file,
						TreeItemValueHolder.ItemType.FILE);
				
				// Add it to the tree
				TreeItem<TreeItemValueHolder> fileTreeItem = new TreeItem<>(fileItem);
				parentItem.getChildren().add(fileTreeItem);
			}
		}
	}
}