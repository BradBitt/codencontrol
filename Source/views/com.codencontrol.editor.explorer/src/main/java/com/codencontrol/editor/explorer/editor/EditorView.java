package com.codencontrol.editor.explorer.editor;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

import org.apache.commons.io.IOUtils;

import com.codencontrol.editor.explorer.listener.TextChangeListener;
import com.codencontrol.ui.dialog.PopUp;
import com.codencontrol.ui.view.CommonView;

import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TextArea;
import javafx.scene.image.ImageView;

public class EditorView extends CommonView {
	
	private File associatedFile;
	private TextChangeListener textChangeListener;
	private TextArea editorTextArea;

	/**
	 * The constructor for the editor view.
	 * @param viewTitle
	 * @param icon
	 * @param id
	 * @param file
	 * @throws IllegalArgumentException
	 */
	public EditorView(String viewTitle, ImageView icon, String id,
			File file) throws IllegalArgumentException {
		
		super(viewTitle, icon, id);
		associatedFile = file;
	}

	@Override
	public void saveViewState() {
		// Nothing is done here.
	}

	/**
	 * This function is called when the view is opened.
	 * This creates a text area and adds the text from the file to it.
	 */
	@Override
	public void run() {
		// Creates a text area box.
		editorTextArea = new TextArea();
		// Styles the text area
		editorTextArea.setId("EditorTextArea");
		editorTextArea.prefWidthProperty().bind(this.widthProperty());
		editorTextArea.prefHeightProperty().bind(this.heightProperty());
		
		// Reads the contents of the file and copies it into the text area.
		if (associatedFile == null || !associatedFile.exists()) {
			return;
		}
		try {
			InputStream fileStream = new FileInputStream(associatedFile);
			editorTextArea.setText(IOUtils.toString(fileStream, Charset.forName("UTF-8")));
		} catch (IOException e) {
			PopUp.showPopUpWithException(AlertType.ERROR, "File Not Found!",
					"The contents of the file could not be loaded.", e);
			return;
		}
		
		this.getChildren().add(editorTextArea);
		
		// Add styling
		addWidthAndHeightStyling();
		
		// Adds a listener to the text area
		// Gets called whenever there is a change made to 
		// an editor.
		textChangeListener = new TextChangeListener(associatedFile, editorTextArea);
		editorTextArea.textProperty().addListener(textChangeListener);
	}
	
	/**
	 * Returns the change listener that is part of this editor view.
	 * @return TextChangeListener
	 */
	public TextChangeListener getTextChangeListener() {
		return textChangeListener;
	}
	
	/**
	 * Returns the file associated with this editor.
	 * @return File
	 */
	public File getAssociatedFile() {
		return associatedFile;
	}
	
	/**
	 * This function returns the text area of this editor view.
	 * @return TextArea
	 */
	public TextArea getTextArea() {
		return editorTextArea; 
	}
}