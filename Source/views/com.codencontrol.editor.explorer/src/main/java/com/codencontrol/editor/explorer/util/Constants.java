package com.codencontrol.editor.explorer.util;

public final class Constants {
	
	public static String FILE_EXPLORER_STYLE_SHEET = "/FileExplorerStyle.css";
	public static String FILE_EXPLORER_NAME = "File Explorer";
	public static String FOLDER_ICON = "/folder_icon.png";
	public static String TEXT_ICON = "/txt_icon.png";
	public static String SAVE_ICON = "/save_icon.png";
	public static String SAVE_DISABLED_ICON = "/save_disabled_icon.png";
	public static String FILE_EXPLORER_ICON = "/file_explorer_icon.png";
	
	public static String FILE_PERSISTANT_PROPERTY = "File";
	public static String NULL_PERSISTANT_PROPERTY = "null";
	
	private Constants() {}
}