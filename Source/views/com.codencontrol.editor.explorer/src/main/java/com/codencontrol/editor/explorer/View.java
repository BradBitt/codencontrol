package com.codencontrol.editor.explorer;

import java.io.File;

import com.codencontrol.editor.explorer.action.AddFolderThread;
import com.codencontrol.editor.explorer.action.OpenFolderHandler;
import com.codencontrol.editor.explorer.util.Constants;
import com.codencontrol.ui.factory.GuiFactory;
import com.codencontrol.ui.stage.MainStage;
import com.codencontrol.ui.view.MainViewContainer.ContainerPosition;
import com.codencontrol.ui.view.UniqueView;

import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

// TODO: Fix weird collapse tree bug.
public class View extends UniqueView {
	
	private OpenFolderHandler openFolderHandler = null;

	/**
	 * Constructor to make the View.
	 * @param viewTitle
	 * @param defaultViewPosition
	 */
	public View(String viewTitle, ContainerPosition defaultViewPosition) {
		super(viewTitle, defaultViewPosition);
		
		// Makes menu items
		makeMenuItems();
		
		// Make tool bar items
		makeToolBarItems();
	}
	
	/**
	 * Uses own icon instead of the default one.
	 * Have to override this function in order to do it.
	 */
	@Override
	public void setIcon() {
		this.icon = new ImageView(
				new Image(View.class.getResourceAsStream(Constants.FILE_EXPLORER_ICON)));
	}

	/**
	 * Function saves values from this view for next time the view
	 * is loaded.
	 */
	@Override
	public void saveViewState() {
		// Adds the file path to the persistent data.
		File associatedFile = openFolderHandler.getAssociatedFile();
		
		if (associatedFile == null) {
			mapData.put(Constants.FILE_PERSISTANT_PROPERTY, Constants.NULL_PERSISTANT_PROPERTY);
		} else {
			String filePath = associatedFile.toString().replace("\\", "/");
			mapData.put(Constants.FILE_PERSISTANT_PROPERTY, filePath);
		}
		
	}

	/**
	 * This function gets run when the view is open for the first time.
	 */
	@Override
	public void run() {
		// Adds style sheet
		GuiFactory.getInstance().getMainStage().getMainScene()
			.getStylesheets().add(
					getClass().getResource(Constants.FILE_EXPLORER_STYLE_SHEET).toExternalForm());
		
		// Add styling
		addWidthAndHeightStyling();
		
		// Checks to see if there was any persistent data saved
		String filePath = mapData.get(Constants.FILE_PERSISTANT_PROPERTY);
		
		// checks it is not null
		if (filePath == null) {
			return;
		}
		if (filePath.equals(Constants.NULL_PERSISTANT_PROPERTY)) {
			return;
		}
		
		// loads the previous directory.
		File oldDirectory = new File(filePath);
		AddFolderThread thread = new AddFolderThread(oldDirectory, this);
		thread.start();
	}
	
	/**
	 * This function creates the menu items to be added to
	 * the main menu in the application.
	 */
	private void makeMenuItems() {
		// Gets the main stage.
		MainStage mainStage = GuiFactory.getInstance().getMainStage();
		
		// Creates the menu to add
		Menu fileExplorerMenu = new Menu("File");
		MenuItem openFolderMenuItem = new MenuItem("Open Folder");
		
		// Adds the view icon to the menu item
		openFolderMenuItem.setGraphic(new ImageView(
				new Image(View.class.getResourceAsStream(Constants.FILE_EXPLORER_ICON))));
		
		openFolderHandler = new OpenFolderHandler(this);
		openFolderMenuItem.setOnAction(openFolderHandler);
		fileExplorerMenu.getItems().add(openFolderMenuItem);
		
		// Adds the folder to the main menu bar
		mainStage.getMainBarContainer().getMenuBar().addMenuItem(fileExplorerMenu);
	}
	
	/**
	 * This function makes the tool bar items.
	 */
	private void makeToolBarItems() {
		// Gets the main stage.
		MainStage mainStage = GuiFactory.getInstance().getMainStage();
				
		// Makes the button
		ToolBarSaveButton saveButton = ToolBarSaveButton.getInstance();
		
		// Adds it to the main tool bar.
		mainStage.getMainBarContainer().getToolBar().addToolBarButton(saveButton);
	}
	
	/**
	 * This function returns the open folder handler used in the view.
	 * It may be null.
	 */
	public OpenFolderHandler getOpenFolderHandler() {
		return openFolderHandler;
	}
	
	/**
	 * Sets the open folder handler.
	 * @param openFolderHandler
	 */
	public void setOpenFolderHandler(OpenFolderHandler openFolderHandler) {
		this.openFolderHandler = openFolderHandler;
	}
}