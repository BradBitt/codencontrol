package com.codencontrol.editor.explorer.listener;

import java.io.File;

import com.codencontrol.editor.explorer.editor.EditorChangeThread;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.TextArea;

public class TextChangeListener implements ChangeListener<String> {
	
	private File associatedFile;
	private TextArea editorTextArea;
	private boolean isChanged = false;
	
	/**
	 * The constructor for the text change listener.
	 * @param associatedFile
	 * @param editorTextArea
	 */
	public TextChangeListener(File associatedFile, TextArea editorTextArea) {
		this.associatedFile = associatedFile;
		this.editorTextArea = editorTextArea;
	}

	/**
	 * This function checks to see if there is differences between
	 * the text in the text area and the file.
	 */
	@Override
	public void changed(ObservableValue<? extends String> var1, String var2, String var3) {
		
		// Runs the check in a separate thread.
		EditorChangeThread thread = new EditorChangeThread(
				associatedFile, editorTextArea, this);
		thread.start();
	}
	
	/**
	 * This function returns whether there is changes
	 * in the editor view or not.
	 * @return boolean
	 */
	public boolean getIsChanged() {
		return isChanged;
	}
	
	/**
	 * This function sets the is changed state of the
	 * linked editor.
	 * @param isChanged
	 */
	public void setIsChanged(boolean isChanged) {
		this.isChanged = isChanged;
	}
}