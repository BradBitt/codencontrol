package com.codencontrol.editor.explorer.listener;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.codencontrol.editor.explorer.View;
import com.codencontrol.editor.explorer.editor.EditorView;
import com.codencontrol.editor.explorer.util.Constants;
import com.codencontrol.ui.dialog.PopUp;
import com.codencontrol.ui.toolbar.ToolBarButton;
import com.codencontrol.ui.util.ViewUtil;
import com.codencontrol.ui.view.MainView;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class SaveListener implements EventHandler<ActionEvent> {
	
	private ToolBarButton saveButton;
	
	/**
	 * The save listener constructor.
	 * @param saveButton
	 */
	public SaveListener(ToolBarButton saveButton) {
		this.saveButton = saveButton;
		saveButton.setDisable(true);
	}

	/**
	 * Saves all the editors that have changes.
	 */
	@Override
	public void handle(ActionEvent var1) {
		ArrayList<EditorView> allEditorViews = new ArrayList<>();
		ArrayList<EditorView> allEditorViewsWithChanges = new ArrayList<>();
		
		// Get all open views
		List<MainView> views = ViewUtil.findAllViews();
		
		// Gets all views that are editor views.
		for (MainView view : views) {
			if (view instanceof EditorView) {
				allEditorViews.add((EditorView) view);
			}
		}
		
		// Checks to see if there is any changes
		for (EditorView editorView : allEditorViews) {
			if (editorView.getTextChangeListener().getIsChanged()) {
				allEditorViewsWithChanges.add(editorView);
			}
		}
		
		// Saves all the changes in the editor to the file.
		for (EditorView editorViewWithChanges : allEditorViewsWithChanges) {
			try {
				saveEditorContents(editorViewWithChanges);
			} catch (IOException e) {
				PopUp.showPopUpWithException(
						AlertType.ERROR,
						"Failed to save Contents to File!",
						"Failed to save the contents of '" +
								editorViewWithChanges.getAssociatedFile().getName() 
								+ "' to the file!",
						e);
			}
		}
		
		// Updates the state of the button
		updateState();
	}
	
	/**
	 * Updates the state of the save button icon.
	 */
	public void updateState() {
		// Checks all the common views for if there is any changes
		boolean thereIsChange = isThereChanges();
		
		// If not then disable the button
		// else enable it.
		if (thereIsChange) {
			saveButton.setDisable(false);
			saveButton.setGraphic(new ImageView(new Image(
					View.class.getResourceAsStream(Constants.SAVE_ICON))));
		} else {
			saveButton.setDisable(true);
			saveButton.setGraphic(new ImageView(new Image(
					View.class.getResourceAsStream(Constants.SAVE_DISABLED_ICON))));
		}
	}
	
	/**
	 * Checks to see if there is changes in any of the
	 * editor views.
	 * @return boolean
	 */
	private boolean isThereChanges() {
		ArrayList<EditorView> allEditorViews = new ArrayList<>();
		
		// Get all open views
		List<MainView> views = ViewUtil.findAllViews();
		
		// Gets all views that are editor views.
		for (MainView view : views) {
			if (view instanceof EditorView) {
				allEditorViews.add((EditorView) view);
			}
		}
		
		// Checks to see if there is any changes
		for (EditorView editorView : allEditorViews) {
			if (editorView.getTextChangeListener().getIsChanged()) {
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * This saves the content of the editor to file.
	 * @param view
	 * @throws IOException 
	 */
	private void saveEditorContents(EditorView view) throws IOException {
		File associatedFile = view.getAssociatedFile();
		String textAreaContents = view.getTextArea().getText();
		
		// Saves the file
		FileWriter writer = new FileWriter(associatedFile, false);
		writer.write(textAreaContents);
		writer.close();
		
		// Removes the is changed boolean from the text content listener.
		view.getTextChangeListener().setIsChanged(false);
	}
}