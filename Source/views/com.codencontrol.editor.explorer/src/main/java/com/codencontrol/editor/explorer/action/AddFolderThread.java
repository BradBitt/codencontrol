package com.codencontrol.editor.explorer.action;

import java.io.File;

import com.codencontrol.editor.explorer.View;
import com.codencontrol.editor.explorer.tree.TreeHolder;

import javafx.application.Platform;

public class AddFolderThread extends Thread {
	
	private TreeHolder folderTree;
	private File selectedDirectory;
	private View mainView;
	
	/**
	 * This constructor provides everything needed to run the
	 * AddFolderThread thread.
	 * @param selectedDirectory
	 * @param mainView
	 */
	public AddFolderThread(File selectedDirectory, View mainView) {
		this.selectedDirectory = selectedDirectory;
		this.mainView = mainView;
		
		// Sets the associated file in the open folder handler.
		mainView.setOpenFolderHandler(new OpenFolderHandler(mainView));
		mainView.getOpenFolderHandler().setAssociatedFile(selectedDirectory);
	}
	
	/**
	 * This function is run in a separate thread to the ui thread.
	 * It collects all the files and create a tree with it and then updates
	 * UI with the created tree.
	 */
	@Override
	public void run() {
		folderTree = new TreeHolder(selectedDirectory);
		// Adds styles for the tree view.
		folderTree.getTree().prefWidthProperty().bind(mainView.widthProperty());
		folderTree.getTree().prefHeightProperty().bind(mainView.heightProperty());
		
		// Updates the UI.
		Platform.runLater(new Runnable() {
            @Override
            public void run() {
				mainView.getChildren().add(folderTree.getTree());
            }
		});
	}
}