package com.codencontrol.editor.explorer.tree;

import java.io.File;

public class TreeItemValueHolder {
	
	private File file;
	private ItemType itemtype;
	
	// Tree item types.
	public enum ItemType {
		FOLDER,
		FILE
	}
	
	/**
	 * This function creates the tree item holder object.
	 * It holds the tree item data.
	 * @param file
	 * @param itemtype
	 */
	public TreeItemValueHolder(File file, ItemType itemtype) {
		this.file = file;
		this.itemtype = itemtype;
	}

	/**
	 * Gets the file associated with the tree item.
	 * @return File
	 */
	public File getFile() {
		return file;
	}

	/**
	 * Gets the item type associated with the tree item.
	 * @return ItemType
	 */
	public ItemType getItemtype() {
		return itemtype;
	}
}