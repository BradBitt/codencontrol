package com.codencontrol.editor.explorer.tree;

import java.io.File;

import org.apache.commons.io.FilenameUtils;

import com.codencontrol.editor.explorer.listener.DoubleClickHandler;
import com.codencontrol.editor.explorer.tree.TreeItemValueHolder.ItemType;
import com.codencontrol.editor.explorer.util.Constants;

import javafx.scene.control.TreeCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class CustomTreeCell extends TreeCell<TreeItemValueHolder> {
	
	private ImageView treeItemGraphic;
	private File associatedFile;
    private ItemType type;
	
	/**
	 * This function updates the tree cell to contain the correct
	 * text and graphic.
	 */
	@Override
    public void updateItem(TreeItemValueHolder treeItem, boolean empty) {
        super.updateItem(treeItem, empty);
        
        if (treeItem == null || empty) {
        	return;
        }
        
        associatedFile = treeItem.getFile();
        type = treeItem.getItemtype();
        
        // Sets the menu item graphic
        treeItemGraphic = findCorrectIcon();
        setGraphic(treeItemGraphic);
        
        // If the file or directory does not exist.
        if (!associatedFile.exists()) {
        	setText("*removed*");
        	return;
        }
        
        // Gets the name of the file and sets that as the text.
        setText(associatedFile.getName());
        
        // Adds listeners
        this.setOnMouseClicked(new DoubleClickHandler(treeItem, this));
    }
	
	/**
	 * Finds the correct icon from the cell.
	 * @param type
	 * @param associatedFile
	 * @return ImageView
	 */
	public ImageView findCorrectIcon() {
		ImageView treeItemGraphic = null;
		
		if (type.equals(ItemType.FILE)) {
        	treeItemGraphic = new ImageView(
        			findImageCorrectImage(associatedFile));
        } else {
        	treeItemGraphic = new ImageView(
        			new Image(CustomTreeCell.class.getResourceAsStream(
        					Constants.FOLDER_ICON)));
        }
		
		return treeItemGraphic;
	}
	
	/**
	 * This function gets the icon image associated with the file extension
	 * of the given file. If the file extension does not have an icon
	 * associated with it then the default text icon is returned.
	 * All image icons should contain the a name format like this:
	 * '{FILE_EXTENSION}_icon.png'
	 * @param associatedFile
	 * @return Image
	 */
	private Image findImageCorrectImage(File associatedFile) {
		String fileExtension = FilenameUtils.getExtension(associatedFile.getName());
		
		// If not file extension was found then use default text icon.
		if (fileExtension == null) {
			return new Image(CustomTreeCell.class.getResourceAsStream(Constants.TEXT_ICON));
		}
		
		// Gets the file extension if it exists, if it doesn't then use the normal text icon.
		try {
			return new Image(this.getClass().getResourceAsStream(fileExtension + "_icon.png"));
		} catch(Exception e) {
			return new Image(CustomTreeCell.class.getResourceAsStream(Constants.TEXT_ICON));
		}
	}
}