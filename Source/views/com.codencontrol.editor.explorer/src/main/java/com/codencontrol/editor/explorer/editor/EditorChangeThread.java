package com.codencontrol.editor.explorer.editor;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

import org.apache.commons.io.IOUtils;

import com.codencontrol.editor.explorer.ToolBarSaveButton;
import com.codencontrol.editor.explorer.listener.TextChangeListener;
import com.codencontrol.ui.dialog.PopUp;

import javafx.application.Platform;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TextArea;

public class EditorChangeThread extends Thread {
	
	private File associatedFile;
	private TextArea editorTextArea;
	private TextChangeListener textChangeListener;
	
	public EditorChangeThread(File associatedFile,
			TextArea editorTextArea, TextChangeListener textChangeListener) {
		this.associatedFile = associatedFile;
		this.editorTextArea = editorTextArea;
		this.textChangeListener = textChangeListener;
	}
	
	/**
	 * This function is run when the check for changes thread begins.
	 */
	@Override
	public void run() {
		boolean isChanged = hasEditorContentsChanged();
		
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				// Updates the state of the save icon
				textChangeListener.setIsChanged(isChanged);
				
				// Updates the button state.
				ToolBarSaveButton.getInstance().getListener().updateState();
			}
		});
	}
	
	/**
	 * This function checks to see if the current editor
	 * has any changes compared to the original file.
	 * It does this in a separate thread.
	 * @return boolean
	 */
	private boolean hasEditorContentsChanged() {
		// Gets the files contents
		if (associatedFile.canRead()) {
			try {
				InputStream fileStream = new FileInputStream(associatedFile);
				String contents = IOUtils.toString(
						fileStream, Charset.forName("UTF-8"));
				
				// Updates the state of the save icon
				return !contents.equals(editorTextArea.getText());
			} catch (IOException e) {
				
				// Adds a pop up window in the UI thread.
				Platform.runLater(new Runnable() {
					@Override
					public void run() {
						PopUp.showPopUpWithException(
								AlertType.ERROR,
								"Failed to update save state!",
								"Failed to update the state of the save button. "
								+ "This could be from comparing the file and "
								+ "the content in the editor.", e);
					}
				});
				return true;
			}
		}
		return true;
	}
}