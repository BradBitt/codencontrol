package com.codencontrol.editor.explorer;

import com.codencontrol.editor.explorer.listener.SaveListener;
import com.codencontrol.editor.explorer.util.Constants;
import com.codencontrol.ui.toolbar.ToolBarButton;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class ToolBarSaveButton extends ToolBarButton {
	
	private static ToolBarSaveButton instance = null;
	private SaveListener listener;

	/**
	 * The constructor for the save button.
	 * This is a singleton class.
	 * @param graphic
	 */
	private ToolBarSaveButton(ImageView graphic) {
		super(graphic);
		listener = new SaveListener(this);
		this.setOnAction(listener);
	}
	
	/**
	 * Gets the only save button instance.
	 * @return ToolBarSaveButton
	 */
	public static ToolBarSaveButton getInstance() {
		if (instance == null) {
			ImageView icon = new ImageView(new Image(
					View.class.getResourceAsStream(Constants.SAVE_DISABLED_ICON)));
			instance = new ToolBarSaveButton(icon);
		}
		return instance;
	}
	
	/**
	 * Returns the save listener linked to this button.
	 * @return SaveListener
	 */
	public SaveListener getListener() {
		return listener;
	}
}