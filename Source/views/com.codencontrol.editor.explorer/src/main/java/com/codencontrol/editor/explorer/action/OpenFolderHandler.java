package com.codencontrol.editor.explorer.action;

import java.io.File;

import com.codencontrol.editor.explorer.View;
import com.codencontrol.ui.factory.GuiFactory;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.DirectoryChooser;

public class OpenFolderHandler implements EventHandler<ActionEvent> {
	
	private View mainView;
	private File selectedDirectory = null;
	
	/**
	 * Creates the open folder handler.
	 * Includes the view object so its UI can be updated.
	 * @param mainView
	 */
	public OpenFolderHandler(View mainView) {
		this.mainView = mainView;
	}

	/**
	 * This function gets called when the 'open folder'
	 * button is clicked from the menu.
	 */
	@Override
	public void handle(ActionEvent event) {
		// Opens the folder directory window.
		DirectoryChooser directoryChooser = new DirectoryChooser();
		selectedDirectory = directoryChooser.showDialog(
				GuiFactory.getInstance().getMainStage());
		
		// If the file object is not a directory then return.
		if (!selectedDirectory.isDirectory()) {
			return;
		}
		
		// Clears the current view of its contents.
		mainView.removeViewUI();
		
		// Adds the new folder tree to the view.
		AddFolderThread thread = new AddFolderThread(selectedDirectory, mainView);
		thread.start();
	}
	
	/**
	 * This returns the most recent selected directory.
	 * @return File
	 */
	public File getAssociatedFile() {
		return selectedDirectory;
	}
	
	/**
	 * This function sets the associated File.
	 * @param selectedDirectory
	 */
	public void setAssociatedFile(File selectedDirectory){
		this.selectedDirectory = selectedDirectory;
	}
}