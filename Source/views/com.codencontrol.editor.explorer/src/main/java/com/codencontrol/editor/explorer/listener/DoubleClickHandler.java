package com.codencontrol.editor.explorer.listener;

import java.io.File;

import com.codencontrol.editor.explorer.editor.EditorView;
import com.codencontrol.editor.explorer.tree.CustomTreeCell;
import com.codencontrol.editor.explorer.tree.TreeItemValueHolder;
import com.codencontrol.editor.explorer.tree.TreeItemValueHolder.ItemType;
import com.codencontrol.ui.util.ViewUtil;
import com.codencontrol.ui.view.MainViewContainer;
import com.codencontrol.ui.view.MainViewContainer.ContainerPosition;

import javafx.event.EventHandler;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

public class DoubleClickHandler implements EventHandler<MouseEvent> {
	
	private TreeItemValueHolder treeItem;
	private CustomTreeCell cell;
	
	public DoubleClickHandler(TreeItemValueHolder treeItem, CustomTreeCell cell) {
		this.treeItem = treeItem;
		this.cell = cell;
	}

	/**
	 * This function is called whenever a item in the tree view
	 * is clicked.
	 */
	@Override
	public void handle(MouseEvent mouseEvent) {
		
		// Exits the function if the tree item is a folder.
		if (treeItem.getItemtype().equals(ItemType.FOLDER)) {
			return;
		}
		
		// Detects if the event is a double click.
		if (mouseEvent.getClickCount() == 2) {
        	
			// Creates the common editor view
			try {
				createView();
			} catch(IllegalArgumentException e) {
				// Returns and does nothing as the view is already open.
				return;
			}
        }
	}
	
	/**
	 * This function creates the common view
	 * when the tree item gets clicked. It only creates the view if the view
	 * is not already open.	
	 */
	private void createView() {
		// Gets the file from the tree item.
		File file = treeItem.getFile();
		// Gets the icon from the tree item.
		ImageView image = cell.findCorrectIcon();
		
		// Creates the view.
		EditorView editorView = new EditorView(
				file.getName(),
				image,
				file.toString(),
				file);
		
		// Adds the view to the default container.
		MainViewContainer mainViewContainer = ViewUtil.findViewContainer(
				ContainerPosition.CENTRE);
		
		mainViewContainer.addView(editorView);
		editorView.run();
	}
}