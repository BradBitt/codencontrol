## What is CodeNControl

This is a large Java Application that allows the ability to editor java source code and possible support for other editor functionality. Yes this is another IDE.

I have created this Java application using the JavaFX GUI.

This java application is very extensable. It has a core set of Java Source code and all other functionality is added to it like plugins.
This means that all the plugins work dependently from eachother and different plugins can be enabled by the user. This makes the application much easier to maintain.

This is developed in an eclipse environment and I have used skills i have learnt on my year in industry placement. Including using Maven to Build my application and using a full set of tests and using Git as a version control system.

## Dependencies
### Build Dependencies
To build the dependencies run the set up scripts within tools. This will build all the dependencies so the program will compile.
#### JRegistry
This jar is used to access the registry.

URL: [https://jregistry.sourceforge.io/](https://jregistry.sourceforge.io/)