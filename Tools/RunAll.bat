@echo off

:: Builds the dependencies.
call 1-SetupDependencies.bat

:: Builds the application with maven.
call 2-MavenBuild.bat

:: Compiles the installer
call 3-CompileInstaller.bat