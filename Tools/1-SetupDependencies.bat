@echo off

:: Adds the JRegistry plugin to the maven repository.
mvn install:install-file ^
    -Dfile="%~dp0../Dependencies/jregistry/jregistry-1.8.3.jar" ^
    -DgroupId=com.jregistry ^
    -DartifactId=jregistry ^
    -Dversion=1.8.3 ^
    -Dpackaging=jar ^
    -DgeneratePom=true


:: FIXME Some reason does not work on new machine

:: Adds the dll files as the JRegistry plugin requries them in the same dir.
robocopy "%~dp0../Dependencies/jregistry/" "%UserProfile%/.m2/repository/com/jregistry/jregistry/1.8.3/" reg.dll reg_x64.dll