@echo off

:: Copies the application exe over to the installer resource folder.
robocopy "%~dp0../Source/core/com.codencontrol.core/target/" "%~dp0../Installer/resources/" CodeNControl.exe

:: Compiles the installer
ISCC "%~dp0../Installer/main.iss"